# Waterfall
Waterfall is a tool to generate Mermaid diagram scripts starting from Java source code. (for now...)  
It uses a Rule Engine to fire a set of rule provided to the app against a set of Facts (in this case the tree of Java classes in a project)  

Waterfall has Git capabilities in order to clone a repository and then inject it as the Fact into the Rule engine.  
It also expose metrics to count success/fail requests

## Introduction
While developing code absorb most of the time of a developer, documentation is very important when building a product, software or not.  
Who is going to use your product needs to know how it works and how to communicate with it.  

Diagrams open the possibilities to show from a different perspective any concept.  

For Documentation details, please refer to our [Documentation Section](documentation/)  

## Versions
Refer to our [ChangeLog Page](documentation/ChangeLog.md)  
Refer to our [MermaideVersionScrip Page](documentation/MermaidVersionScripts.md)  

## Functionality
Business:  
-  MVEL rule  
-  Simple Mermaid diagram generation (Shapes and links)  
-  SubGraphDiagram Mermaid diagram generation (Package dependency diagram)  

Tech:  
-  Logging  
-  Exception management  
-  Swagger  
-  Git integration  
-  Database (Cockroach)  
-  Metrics  

## Technologies
a list of technologies used in this project.    

### Mermaid 
Used in the UI project. It is the library that parse the script generated from this project.  
 
Have a look to the [GitPage](https://mermaid-js.github.io/mermaid/#/)  
Documentation of Charts [here](https://mermaid-js.github.io/mermaid/#/flowchart)  

### Grafana
Grafana is a Cloud monitoring tool. In this project it is used to monitor the endpoints.  
Get more details in the [Grafana Page](documentation/tech/Grafana.md)  

Have a look to the [Official Page](https://grafana.com/grafana/)  

## Dependencies

#### Qdox 
Qdox is a java parer that give an abstract representation of a java source tree.    
Used to read properties, methods, annotations and more from source code.  
Have a look to the [GitPage](https://github.com/paul-hammant/qdox)  

#### Easy-Rule
Get more details in the [EasyRule Page](documentation/tech/EasyRule.md)  
Easy-Rule is a minimal set of classes to implement a rule engine in Java.  
Used to query the source code and generate diagram elements.  
> Add a diagram that shows rule engine with facts and rules injection

Have a look to the [GitPage](https://github.com/j-easy/easy-rules)  

#### JGit  
JGit is a simple library that implement a full Git layer.  
Used to clone a repository and finally have available the source code for the Rule engine.  

Have a look to the [GitPage](https://www.eclipse.org/jgit/)  

#### Swagger  
Swagger is a tool to expose documentation of REST endpoint.  
Used to present a HTML page with forms to interact and test the application  

Page is usually following this pattern: `http://localhost:8080/swagger-ui.html#/`

Have a look to the [HomePage](https://swagger.io/)  

#### Prometheus/Micrometer - (Grafana)  
Prometheus is an open source monitoring system.  
Used to expose metrics of the application in paraller with Micrometer.  

Have a look to the [HomePage](https://prometheus.io/)  

### Formats

#### MVEL
MVEL is a java-based business languages. It is used to write rules, particularly Condition and Actions.  

Get more details in the [MVELRule Page](documentation/MVELRule.md)  

#### JSON
Used as input payload.  


## Diagrams
Component diagram  
![Diagram](documentation/images/componentDiagramV1.0.2.png "Component Diagram")  

Waterfall Vision  
![Diagram](documentation/images/WaterfallVision.png "Waterfall Vision")  

## References
Easy-Rule Wiki page: https://github.com/j-easy/easy-rules/wiki  
Easy-Rule Tutorial 1: https://www.programmersought.com/article/7421789546/  
Easy-Rule Tutorial 2: https://programmer.ink/think/java-rules-engine-easy-rules.html  
Easy-Rule Slide 1: https://speakerdeck.com/benas/easy-rules?slide=8  
JGit tutorial1: https://www.vogella.com/tutorials/JGit/article.html  

Java Parser: https://www.beyondjava.net/a-simple-class-parser (have a look!)  

have a look to Jeasy products https://www.jeasy.org/  