package com.deloitte.graphBuilder.service;

import com.deloitte.graphBuilder.packagediagram.InPayload;
import com.thoughtworks.qdox.model.JavaPackage;
import com.thoughtworks.qdox.model.JavaSource;

public interface IBuilder {
    void init(InPayload params);
    void addPackage(JavaPackage pkg);
    void addPackage(String pkgName);
    void addClass(JavaSource src);
}
