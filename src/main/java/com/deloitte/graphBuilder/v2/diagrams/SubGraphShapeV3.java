package com.deloitte.graphBuilder.v2.diagrams;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
@EqualsAndHashCode(callSuper=true)
public class SubGraphShapeV3 extends ShapeV2{

    private List<SubGraphShapeV3> pkgs;
    private List<ShapeV2> nodes;

    @Builder
    public SubGraphShapeV3(List<SubGraphShapeV3> pkgs, List<ShapeV2> nodes, String id, String name){
        super(0, id, name);
        this.pkgs = pkgs;
        this.nodes = nodes;
    }

    public SubGraphShapeV3 addSubGraph(String subGraph, String separator){
        return  addSubGraphWithPath(subGraph, "", separator);
    }

    //this method create packages with the id equal to the previous package part
    // i.e. if arrive com.deloitte.model the package model will have the name as "model" but the id as "com.deloitte.model"
    public SubGraphShapeV3 addSubGraphWithPath(String subGraph, String sourcePkg, String separator){
        SubGraphShapeV3 result;
        String subGraphName = extractPackage(subGraph, separator);

        // if the node exist, select it
        if (pkgs.stream().filter(p -> p.getName().equals(subGraphName)).findFirst().isPresent()) {
            result = pkgs.stream().filter(p -> p.getName().equals(subGraphName)).findFirst().get();
        }
        else{ // otherwise create a new node and add it to the list of subGraph
            result = SubGraphShapeV3.builder()
                .pkgs(new ArrayList<>())
                .nodes(new ArrayList<>())
                .name(subGraphName)
                .id(UUID.randomUUID().toString()).build();
            result.getNodes().add(new ShapeV2(0, sourcePkg + separator + subGraphName, subGraphName));
            pkgs.add(result);
        }

        if (subGraph.contains(separator))
            result = result.addSubGraphWithPath(subGraph.substring(subGraph.indexOf(separator) + 1),
                sourcePkg + separator + subGraphName,
                separator);

        return result;
    }

    public void addNodeWithPath(ShapeV2 aShape, String pathToNode, String separator){

        if(pathToNode.contains(separator)) {
            String path = pathToNode.substring(0, pathToNode.lastIndexOf(separator));

            // create sub graph nodes if they don't exist
            addSubGraphWithPath(path, "", separator);
        }

        String subGraphName = extractPackage(pathToNode, separator);

        // at this point the subgraph exists, just select them from tree of nodes.
        if (pathToNode.contains(separator))
            pkgs.stream().filter(p -> p.getName().equals(subGraphName)).findFirst().get()
                .addNodeWithPath(aShape, pathToNode.substring(pathToNode.indexOf(separator)+1), separator);
        else
            addShape(aShape);
    }

    public ShapeV2 createShape(String id, String name, String color){
        return new ShapeV2(0, id, name, color);
    }

    private void addShape(ShapeV2 clsShape){
        this.nodes.add(clsShape);
    }

    private String extractPackage(String pkg, String separator){
        if(pkg.contains(separator))
            return pkg.substring(0, pkg.indexOf(separator));
        else
            return pkg;
    }

}
