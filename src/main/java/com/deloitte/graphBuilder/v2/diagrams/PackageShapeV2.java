package com.deloitte.graphBuilder.v2.diagrams;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class PackageShapeV2 extends ShapeV2{

    private List<PackageShapeV2> pkgs;
    private List<ShapeV2> nodes;

    @Builder
    public PackageShapeV2(List<PackageShapeV2> pkgs, List<ShapeV2> nodes, String id, String name){
        super(0, id, name);
        this.pkgs = pkgs;
        this.nodes = nodes;
    }

    public void addPackage(String pkg){
        String pkgName = extractPackage(pkg);

        PackageShapeV2 pkgShape;
        if (pkgs.stream().filter(p -> p.getName().equals(pkgName)).findFirst().isPresent()) {
            pkgShape = pkgs.stream().filter(p -> p.getName().equals(pkgName)).findFirst().get();
        }
        else{
            pkgShape = PackageShapeV2.builder()
                .pkgs(new ArrayList<>())
                .nodes(new ArrayList<>())
                .name(pkgName)
                .id(UUID.randomUUID().toString()).build();
            pkgShape.getNodes().add(new ShapeV2(0, UUID.randomUUID().toString(), pkgName));
            pkgs.add(pkgShape);
        }

        if (pkg.contains("."))
            pkgShape.addPackage(pkg.substring(pkg.indexOf(".") + 1));
    }

    //this method create packages with the id equal to the previous package part
    // i.e. if arrive com.deloitte.model the package model will have the name as "model" but the id as "com.deloitte.model"
    public void addPackageWithSource(String pkg, String sourcePkg){
        String pkgName = extractPackage(pkg);

        PackageShapeV2 pkgShape;
        if (pkgs.stream().filter(p -> p.getName().equals(pkgName)).findFirst().isPresent()) {
            pkgShape = pkgs.stream().filter(p -> p.getName().equals(pkgName)).findFirst().get();
        }
        else{
            pkgShape = PackageShapeV2.builder()
                .pkgs(new ArrayList<>())
                .nodes(new ArrayList<>())
                .name(pkgName)
                .id(UUID.randomUUID().toString()).build();
            pkgShape.getNodes().add(new ShapeV2(0, sourcePkg+"."+pkgName, pkgName));
            pkgs.add(pkgShape);
        }

        if (pkg.contains("."))
            pkgShape.addPackageWithSource(pkg.substring(pkg.indexOf(".") + 1), sourcePkg+"."+pkgName);
    }

    public void addClassInPackage(ShapeV2 clsShape, String pkg){
        String pkgName = extractPackage(pkg);
        if (pkg.contains("."))
            pkgs.stream().filter(p -> p.getName().equals(pkgName)).findFirst().get()
                    .addClassInPackage(clsShape, pkg.substring(pkg.indexOf(".")+1));
        else
            pkgs.stream().filter(p -> p.getName().equals(pkgName)).findFirst().get()
                    .addClass(clsShape);
    }

    private void addClass(ShapeV2 clsShape){
        this.nodes.add(clsShape);
    }

    private String extractPackage(String pkg){
        if(pkg.contains("."))
            return pkg.substring(0, pkg.indexOf("."));
        else
            return pkg;
    }

}
