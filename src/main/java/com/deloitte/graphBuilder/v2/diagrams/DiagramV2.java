package com.deloitte.graphBuilder.v2.diagrams;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DiagramV2 {
    private List<ShapeV2> shapes = new ArrayList<>();
    private List<LinkShapeV2> links = new ArrayList<>();

    public void addShape(int position, String id, String name){
        shapes.add(new ShapeV2(position, id, name));
    }
    public void addShape(int position, String id, String name, String color){
        shapes.add(new ShapeV2(position, id, name, color));
    }

    public void addLink(String name, String sourceNode, String destNode){
        links.add(LinkShapeV2.builder()
            .name(name)
            .sourceNode(sourceNode)
            .destNode(destNode)
            .build());
    }
}
