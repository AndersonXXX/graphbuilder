package com.deloitte.graphBuilder.v2.diagrams;

import com.thoughtworks.qdox.model.JavaSource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubGraphDiagram {

    private List<PackageShapeV2> pkgs = new ArrayList<>();
    private Set<LinkShapeV2> linkSet = new HashSet<>();

    public void addPackage(String pkg){
        String pkgName = extractPackage(pkg);

        PackageShapeV2 pkgShape;
        if (pkgs.stream().filter(p -> p.getName().equals(pkgName)).findFirst().isPresent()) {
            pkgShape = pkgs.stream().filter(p -> p.getName().equals(pkgName)).findFirst().get();
        }
        else{
            pkgShape = PackageShapeV2.builder()
                .pkgs(new ArrayList<>())
                .nodes(new ArrayList<>())
                .name(pkgName)
                .id(UUID.randomUUID().toString()).build();
            pkgShape.getNodes().add(new ShapeV2(0, pkgName, pkgName));
            pkgs.add(pkgShape);
        }

        if (pkg.contains("."))
            pkgShape.addPackageWithSource(pkg.substring(pkg.indexOf(".") + 1), pkgName);
    }

    public void addClass(JavaSource src){
        String pkgName = src.getPackageName();

        System.out.println("Class: " + src.getClasses().get(0).getName());
        for ( String imp : src.getImports()) {
            linkSet.add(new LinkShapeV2("0", pkgName, imp.substring(0, imp.lastIndexOf('.')), ""));
        }
    }

    private String extractPackage(String pkg){
        if(pkg.contains("."))
            return pkg.substring(0, pkg.indexOf("."));
        else
            return pkg;
    }

}
