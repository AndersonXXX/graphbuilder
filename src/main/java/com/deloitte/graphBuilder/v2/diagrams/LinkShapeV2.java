package com.deloitte.graphBuilder.v2.diagrams;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class LinkShapeV2 extends ShapeV2{

    private String sourceNode, destNode;

    @Builder
    public LinkShapeV2(String sourceNode, String destNode, String name){
        super(0, "id", name);
        this.sourceNode = sourceNode;
        this.destNode = destNode;
    }

    @Builder
    public LinkShapeV2(String id, String sourceNode, String destNode, String name){
        super(0, id, name);
        this.sourceNode = sourceNode;
        this.destNode = destNode;
    }


}
