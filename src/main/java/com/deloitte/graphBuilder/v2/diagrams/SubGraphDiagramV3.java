package com.deloitte.graphBuilder.v2.diagrams;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class SubGraphDiagramV3 {

    private String title;
    private SubGraphShapeV3 rootGraph;
    private List<SubGraphShapeV3> subGraphs = new ArrayList<>();
    private Set<LinkShapeV2> linkSet = new HashSet<>();

    public SubGraphDiagramV3(){
        this("No Title");
    }

    public SubGraphDiagramV3(String title){
        this.title = title;
        rootGraph = SubGraphShapeV3.builder()
            .pkgs(new ArrayList<>())
            .nodes(new ArrayList<>())
            .name(title)
            .id(UUID.randomUUID().toString()).build();
        subGraphs.add(rootGraph);
    }

    public void addSubGraph(String subGraph, String separator){
        rootGraph.addSubGraph(subGraph, separator);
    }

    public ShapeV2 addNodeWithPath(String pathToNode, String separator){
        String nodeName = pathToNode.substring(pathToNode.lastIndexOf(separator) + 1);

        ShapeV2 node = new ShapeV2(0, pathToNode, nodeName);
        rootGraph.addNodeWithPath(node, pathToNode, separator);

        return node;
    }

    public void addLink(String name, String sourceNode, String destNode){
        linkSet.add(LinkShapeV2.builder()
            .name(name)
            .sourceNode(sourceNode)
            .destNode(destNode)
            .build());
    }

}
