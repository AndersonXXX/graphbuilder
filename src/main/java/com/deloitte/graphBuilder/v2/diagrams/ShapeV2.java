package com.deloitte.graphBuilder.v2.diagrams;

import lombok.Data;

@Data
public class ShapeV2 {
    private int position;
    private String id, name, color;

    public ShapeV2(int position, String name){
        this.position = position;
        this.id = name;
        this.name = name;
    }

    public ShapeV2(int position, String id, String name){
        this(position, name);
        this.id = id;
    }

    public ShapeV2(int position, String id, String name, String color){
        this(position, id, name);
        this.color = color;
    }
}
