package com.deloitte.graphBuilder.v2.mveldiagram;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class InPayloadMVEL {

    @NotNull
    @ApiModelProperty(value = "This property is the public URI where the repository is exposed and available to be fetched",
        example = "https://AndersonXXX@bitbucket.org/AndersonXXX/graphbuilder.git")
    private String repositoryURL;

    @ApiModelProperty(value = "a reference to the branch you want to be checked out (leave blank for master)",
        example = "master")
    private String branchName;

    @NotNull
    @ApiModelProperty(value = "Local path containing the project. part of old implementation. Git has priority. (deprecated)",
        example = "/Users/mdellarovere/Projects/workspace/fraud-aml-manager/src/main/java/com/tenx/fraudamlmanager")
    private String sourcePath;

    @NotNull
    @ApiModelProperty(value = "this indicate which is the root folder to start parsing classes",
        example = "src/main/java/com/tenx/fraudamlmanager")
    private String rootPath;

    @ApiModelProperty(value = "if you want the payload to be saved on DB, set this flag to true. (Do it when your query works)",
        example = "true")
    private boolean savePayload;

    @NotNull
    @ApiModelProperty(value = "this are the parameters for the rule. Condition, Actions, Name, Description and Priority")
    List<MVELPayload> mvelRuleList;

}
