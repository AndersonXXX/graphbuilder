package com.deloitte.graphBuilder.v2.mveldiagram.subgraphdiagram;

import com.deloitte.graphBuilder.v2.db.MVELRepoService;
import com.deloitte.graphBuilder.v2.exceptions.GitException;
import com.deloitte.graphBuilder.v2.exceptions.MVELParsingException;
import com.deloitte.graphBuilder.v2.exceptions.QDOXParsingException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/api/v2/subgraph")
@RequiredArgsConstructor
@Api(tags = "MVEL")
@Slf4j
public class SubGraphDiagramController {

    private final SubGraphDiagramService service;
    private final MVELRepoService repoService;

    @ApiOperation(value = "Endpoint to generate a subgraph diagram of the application with MVEL rules")
    @PostMapping("/getSubGraphDiagram")
    public String getSubGraphDiagram(@RequestBody @Valid SubGraphDiagramPayload params)
        throws MVELParsingException, QDOXParsingException, GitException {
        log.info("Call received - getSubGraphDiagram");
        service.process(params);
        return service.getDiagram();
    }

}