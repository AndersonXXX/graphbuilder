package com.deloitte.graphBuilder.v2.mveldiagram.markdowndiagram;

import com.deloitte.graphBuilder.v2.adapters.markdown.MarkDownAdapter;
import com.deloitte.graphBuilder.v2.adapters.metrics.Metrics;
import com.deloitte.graphBuilder.v2.diagrams.SubGraphDiagramV3;
import com.deloitte.graphBuilder.v2.exceptions.MVELParsingException;
import com.deloitte.graphBuilder.v2.mveldiagram.InPayloadMVEL;
import com.deloitte.graphBuilder.v2.rules.RuleManager;
import com.deloitte.graphBuilder.v2.serialisers.MermaidSubDiagramSerialiser;
import com.deloitte.graphBuilder.v2.utils.FileUtil;
import java.io.File;
import java.io.IOException;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class MarkDownServiceSubGraph {

    private MermaidSubDiagramSerialiser serialiser;
    private SubGraphDiagramV3 diagram;
    private Metrics counter;

    @Autowired
    public MarkDownServiceSubGraph(Metrics counter) {
        this.serialiser = new MermaidSubDiagramSerialiser();
        this.counter = counter;
    }

    public void process(InPayloadMVEL params)
        throws MVELParsingException, IOException {

        log.info("Start processing.");

        try{
            // get the collection of MD file from project folder
            log.info("Reading files...");
            String projName = params.getRepositoryURL().substring(
                params.getRepositoryURL().lastIndexOf("/")+1,
                params.getRepositoryURL().indexOf(".git"));

            List<File> files = FileUtil.getMDlist(FileUtil.getBasePath() + "Waterfall/workspace/" + projName);

            // Create the source manager and diagram to be added as fact. The will be available during MVEL manipulation.
            diagram = new SubGraphDiagramV3();

            // Initialise the engine
            log.info("Creating Engine and adding rules...");
            RuleManager ruleManager = new RuleManager();
            ruleManager.addRules(params.getMvelRuleList());

            // Iterate through the file (classes source code) and fire the engine for each
            log.info("Iterating through classes and firing the Engine");
            for (File f : files) {

                // add fact to the Rule Manager
                ruleManager.addFact("file", new MarkDownAdapter(f));
                ruleManager.addFact("diagram", diagram);

                // fire rules on known facts
                ruleManager.fire();
            }

            counter.incrementMarkDownDiagramV2Succeded();

        } catch (MVELParsingException | IOException e) {
            counter.incrementMarkDownDiagramV2Fail();
            throw e;
        }

        log.info("Diagram created.");
    }

    public String getDiagram() throws MVELParsingException {
        log.info("Exporting Mermaid diagram...");
        try {
            return serialiser.getDiagram(diagram);
        }catch (Exception ex){
            throw new MVELParsingException("Exception during execution.", ex);
        }

    }

}
