package com.deloitte.graphBuilder.v2.mveldiagram;

import com.deloitte.graphBuilder.v2.adapters.git.GitWrapper;
import com.deloitte.graphBuilder.v2.db.MVELRepoService;
import com.deloitte.graphBuilder.v2.db.MVELRuleEntity;
import com.deloitte.graphBuilder.v2.exceptions.GitException;
import com.deloitte.graphBuilder.v2.exceptions.MVELParsingException;
import com.deloitte.graphBuilder.v2.exceptions.QDOXParsingException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/api/v2/mvel")
@RequiredArgsConstructor
@Api(tags = "MVEL")
@Slf4j
public class MVELControllerV2 {

    private final MVELServiceV2 service;
    private final MVELRepoService repoService;
    private final GitWrapper wrapper;

    @ApiOperation(value = "Endpoint to generate the module diagram of the application with MVEL rules")
    @PostMapping("/getModuleDiagram")
    public String getModuleDiagram(@RequestBody @Valid InPayloadMVEL params)
        throws MVELParsingException, QDOXParsingException, GitException {
        log.info("Call received - getModuleDiagramMVEL");
        service.process(params);
        return service.getDiagram();
    }

    @ApiOperation(value = "Endpoint to get the MVEL payload saved on DB for a particular Project")
    @GetMapping("/getPayloadsByProject")
    public List<MVELRuleEntity> getPayloadsByProject(@RequestParam("projectNameOnGit") String projectNameOnGit) {
        log.info("Call received - getPayloadsByProject");
        return repoService.findByProjectName(projectNameOnGit);
    }

    @ApiOperation(value = "get a list of branches for the specific project")
    @GetMapping("/getBranches")
    public List<String> getBranches(@RequestParam("repoURI") String repoURI) throws GitException {
        log.info("Call received - getBranches");
        return wrapper.getRemoteBranches(repoURI);
    }
}