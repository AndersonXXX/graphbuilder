package com.deloitte.graphBuilder.v2.mveldiagram;

import com.deloitte.graphBuilder.v2.adapters.metrics.Metrics;
import com.deloitte.graphBuilder.v2.db.MVELRepoService;
import com.deloitte.graphBuilder.v2.diagrams.DiagramV2;
import com.deloitte.graphBuilder.v2.exceptions.GitException;
import com.deloitte.graphBuilder.v2.exceptions.MVELParsingException;
import com.deloitte.graphBuilder.v2.exceptions.QDOXParsingException;
import com.deloitte.graphBuilder.v2.readers.JavaSourceReader;
import com.deloitte.graphBuilder.v2.rules.RuleManager;
import com.deloitte.graphBuilder.v2.serialisers.IDiagramSerialiser;
import com.thoughtworks.qdox.model.JavaSource;
import java.util.Collection;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class MVELServiceV2 {

    private JavaSourceReader reader;
    private IDiagramSerialiser serialiser;
    private DiagramV2 diagram;
    private Metrics counter;

    MVELRepoService repoService;

    @Autowired
    public MVELServiceV2(@Qualifier("MermaidDiagramSerialiser") IDiagramSerialiser serialiser, JavaSourceReader reader, Metrics counter, MVELRepoService repoService) {
        this.reader = reader;
        this.serialiser = serialiser;
        this.counter = counter;
        this.repoService = repoService;
    }

    public void process(InPayloadMVEL params) throws QDOXParsingException, MVELParsingException, GitException {

        log.info("Start processing.");

        try{
            // get the collection of JavaSource from source code
            log.info("Reading source code...");
            Collection<JavaSource> sources = null;
            if ( params.getRepositoryURL() != null && params.getRepositoryURL() != "")
                sources = reader.readRepoURI(params.getRepositoryURL(), params.getRootPath(), params.getBranchName());
            else
                sources = reader.read(params.getSourcePath());

            // Create the source manager and diagram to be added as fact. The will be available during MVEL manipulation.
            diagram = new DiagramV2();

            // Initialise the engine
            log.info("Creating Engine and adding rules...");
            RuleManager ruleManager = new RuleManager();
            ruleManager.addRules(params.getMvelRuleList());

            // Iterate through the file (classes source code) and fire the engine for each
            log.info("Iterating through classes and firing the Engine");
            for (JavaSource src : sources) {

                // add fact to the Rule Manager. First time it get added, other time it get override.
                ruleManager.addFact("class", src.getClasses().stream().findFirst().get());
                ruleManager.addFact("source", src);
                ruleManager.addFact("diagram", diagram);

                // fire rules on known facts
                ruleManager.fire();
            }

            if(params.isSavePayload())
                repoService.savePayload(params);

            counter.incrementModuleDiagramV2Succeded();

        } catch (MVELParsingException | QDOXParsingException | GitException e) {
            counter.incrementModuleDiagramV2Fail();
            throw e;
        }

        log.info("Diagram created.");
    }

    public String getDiagram() throws MVELParsingException {
        log.info("Exporting Mermaid diagram...");
        try {
            return serialiser.getDiagram(diagram);
        }catch (Exception ex){
            throw new MVELParsingException("Exception during execution.", ex);
        }

    }

}
