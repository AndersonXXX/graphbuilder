package com.deloitte.graphBuilder.v2.mveldiagram.markdowndiagram;

import com.deloitte.graphBuilder.v2.exceptions.MVELParsingException;
import com.deloitte.graphBuilder.v2.mveldiagram.InPayloadMVEL;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.IOException;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/api/v2/mvel")
@RequiredArgsConstructor
@Api(tags = "MVEL")
@Slf4j
public class MarkDownDiagramController {

    private final MarkDownService service;
    private final MarkDownServiceSubGraph serviceSubGraph;

    @ApiOperation(value = "Endpoint to generate a diagram of links present in markdown files (Only links to other MD files)")
    @PostMapping("/getMarkDownDiagram")
    public String getMarkDownDiagram(@RequestBody @Valid InPayloadMVEL params)
        throws MVELParsingException, IOException {
        log.info("Call received - getMarkDownDiagram");
        service.process(params);
        return service.getDiagram();
    }

    @ApiOperation(value = "Endpoint to generate a SubGraph diagram of links present in markdown files (Only links to other MD files)")
    @PostMapping("/getMarkDownSubGraphDiagram")
    public String getMarkDownSubGraphDiagram(@RequestBody @Valid InPayloadMVEL params)
        throws MVELParsingException, IOException {
        log.info("Call received - getMarkDownSubGraphDiagram");
        serviceSubGraph.process(params);
        return serviceSubGraph.getDiagram();
    }
}