package com.deloitte.graphBuilder.v2.mveldiagram;

import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class MVELPayload {

    @NotNull
    @ApiModelProperty(example = "mvel rule")
    String name;

    @NotNull
    @ApiModelProperty(example = "first MVEL test")
    String description;

    @NotNull
    @ApiModelProperty(example = "class.Source.Imports.contains(\"org.springframework.cloud.openfeign.FeignClient\")")
    String condition;

    @NotNull
    List<String> actions;

    @ApiModelProperty(example = "1")
    int priority;

}
