package com.deloitte.graphBuilder.v2.mveldiagram;

public interface IDiagramService {
    void process(InPayloadMVEL params);
}
