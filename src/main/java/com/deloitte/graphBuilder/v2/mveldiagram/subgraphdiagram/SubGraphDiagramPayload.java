package com.deloitte.graphBuilder.v2.mveldiagram.subgraphdiagram;

import com.deloitte.graphBuilder.v2.mveldiagram.MVELPayload;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class SubGraphDiagramPayload {

    @NotNull
    @ApiModelProperty(example = "https://AndersonXXX@bitbucket.org/AndersonXXX/graphbuilder.git")
    private String repositoryURL;

    @ApiModelProperty(example = "master")
    private String branchName;

    @NotNull
    @ApiModelProperty(example = "/Users/mdellarovere/Projects/workspace/fraud-aml-manager/src/main/java/com/tenx/fraudamlmanager")
    private String sourcePath;

    @NotNull
    @ApiModelProperty(example = "src/main/java/com/deloitte/graphBuilder")
    private String rootPath;

    @NotNull
    @ApiModelProperty(example = "com.deloitte.graphbuilder")
    private String pkgDefault;

    @ApiModelProperty(example = "true")
    private boolean savePayload;

    @NotNull
    @ApiModelProperty
    List<MVELPayload> mvelRuleList;

}
