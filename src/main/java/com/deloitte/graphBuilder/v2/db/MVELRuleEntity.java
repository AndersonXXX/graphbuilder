package com.deloitte.graphBuilder.v2.db;

import com.deloitte.graphBuilder.v2.mveldiagram.InPayloadMVEL;
import com.deloitte.graphBuilder.v2.mveldiagram.MVELPayload;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import java.util.Date;
import java.util.HashMap;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "mvel_rules")
@Data
@NoArgsConstructor
@AllArgsConstructor
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class MVELRuleEntity {
    @Id
    @Column(nullable = false)
    private String id;

    @Column(nullable = false)
    private String reposirotyURL;

    @Column(nullable = false)
    private String rootPath;

    @Column(nullable = false)
    private String sourcePath;

    @Column(nullable = false)
    private String projectName;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb", nullable = false)
    private HashMap<String, Object> rules;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date updatedDate;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, updatable = false)
    private Date createdDate;

    public void fill(InPayloadMVEL payload) throws JsonProcessingException {
        rules = new HashMap<>();
        this.reposirotyURL = payload.getRepositoryURL();
        this.rootPath = payload.getRootPath();
        this.sourcePath = payload.getSourcePath();

        String projName = reposirotyURL.substring(reposirotyURL.lastIndexOf("/")+1, reposirotyURL.indexOf(".git"));
        this.projectName = projName;

        ObjectMapper mapper = new ObjectMapper();
        for (MVELPayload r : payload.getMvelRuleList()) {
            rules.put(r.getName(), mapper.writeValueAsString(r));
        }
    }
}