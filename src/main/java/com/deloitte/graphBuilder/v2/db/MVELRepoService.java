package com.deloitte.graphBuilder.v2.db;

import com.deloitte.graphBuilder.v2.mveldiagram.InPayloadMVEL;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.List;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class MVELRepoService {

    private MVELRuleRepository repository;
    private DatabaseMetrics metrics;

    @Autowired
    public MVELRepoService(MVELRuleRepository repository, DatabaseMetrics metrics){
        this.repository = repository;
        this.metrics = metrics;
    }

    public void savePayload(InPayloadMVEL params){
        try {

            MVELRuleEntity entity = new MVELRuleEntity();
            entity.setId(UUID.randomUUID().toString());
            entity.fill(params);

            repository.save(entity);
            metrics.incrementDatabaseInsertSuccess();
        } catch (JsonProcessingException e) {
            metrics.incrementDatabaseInsertFail();
            log.error(e.getMessage());
        }
    }

    public List<MVELRuleEntity> findByProjectName(String projectName){
        return repository.findByProjectName(projectName);
    }
}
