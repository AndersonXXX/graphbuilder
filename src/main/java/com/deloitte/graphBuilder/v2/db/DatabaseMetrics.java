package com.deloitte.graphBuilder.v2.db;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DatabaseMetrics {

    private static final String DATABASE_INSERT_SUCCESS = "database.insert.success";
    private static final String DATABASE_INSERT_FAIL = "database.insert.fail";

    @Autowired
    private MeterRegistry registry;

    private Counter databaseInsertSuccess;
    private Counter databaseInsertFail;

    @PostConstruct
    public void initializeCounter() {
        databaseInsertSuccess = getCounterInstance(DATABASE_INSERT_SUCCESS);
        databaseInsertFail = getCounterInstance(DATABASE_INSERT_FAIL);
    }

    public void incrementDatabaseInsertSuccess() {
        databaseInsertSuccess.increment();
    }
    public void incrementDatabaseInsertFail() {
        databaseInsertFail.increment();
    }

    // Private methods
    private Counter getCounterInstance(String name) {
        return Counter
            .builder(name)
            .register(registry);
    }

}
