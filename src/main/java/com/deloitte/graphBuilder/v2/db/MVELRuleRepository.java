package com.deloitte.graphBuilder.v2.db;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MVELRuleRepository extends JpaRepository<MVELRuleEntity, Long> {

    List<MVELRuleEntity> findByProjectName(String projectName);
}
