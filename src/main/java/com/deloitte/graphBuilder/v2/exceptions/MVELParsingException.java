package com.deloitte.graphBuilder.v2.exceptions;

public class MVELParsingException extends Exception {

    public MVELParsingException(String message, Throwable e){
        super(message, e);
    }

}
