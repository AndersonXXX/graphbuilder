package com.deloitte.graphBuilder.v2.exceptions;

public class GitException extends Exception {

    public GitException(String message, Throwable e){
        super(message, e);

    }

}
