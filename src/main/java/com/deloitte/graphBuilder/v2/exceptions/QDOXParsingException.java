package com.deloitte.graphBuilder.v2.exceptions;

public class QDOXParsingException extends Exception {

    public QDOXParsingException(String message, Throwable e){
        super(message, e);
    }

}
