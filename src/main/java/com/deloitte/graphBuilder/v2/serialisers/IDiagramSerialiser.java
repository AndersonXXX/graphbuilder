package com.deloitte.graphBuilder.v2.serialisers;

import com.deloitte.graphBuilder.v2.diagrams.DiagramV2;

public interface IDiagramSerialiser {
    String getDiagram(DiagramV2 diagram);
}
