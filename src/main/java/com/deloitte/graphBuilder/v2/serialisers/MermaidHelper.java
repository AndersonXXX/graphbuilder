package com.deloitte.graphBuilder.v2.serialisers;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MermaidHelper {

    public static String formatMermaidScript(String script) {

        log.info("Start formatting the Script.");
        String result = "";
        String[] lines = script.split(System.lineSeparator());

        int tabs = 0;
        for (String line : lines) {

            if (line.trim().equals("end"))
                tabs--;

            result = result + getTabs(tabs) + line.trim() + System.lineSeparator();

            if (line.trim().startsWith("subgraph"))
                tabs++;
        }

        return result;
    }

    private static String getTabs(int tabs){
        String result = "";
        for (int i = 0; i<tabs; i++)
            result += "\t";
        return result;
    }
}
