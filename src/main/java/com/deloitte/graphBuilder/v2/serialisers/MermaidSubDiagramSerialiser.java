package com.deloitte.graphBuilder.v2.serialisers;

import com.deloitte.graphBuilder.v2.diagrams.LinkShapeV2;
import com.deloitte.graphBuilder.v2.diagrams.PackageShapeV2;
import com.deloitte.graphBuilder.v2.diagrams.ShapeV2;
import com.deloitte.graphBuilder.v2.diagrams.SubGraphDiagram;
import com.deloitte.graphBuilder.v2.diagrams.SubGraphDiagramV3;
import com.deloitte.graphBuilder.v2.diagrams.SubGraphShapeV3;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("MermaidSubDiagramSerialiser")
public class MermaidSubDiagramSerialiser {

    // ***********************
    // *** SubGraphDiagram ***
    // ***********************
    public String getDiagram(SubGraphDiagram diagram, String pkgDefault){
        StringBuilder builder = new StringBuilder();
        builder.append("graph LR").append(";").append(System.lineSeparator());

        for (PackageShapeV2 pkg : diagram.getPkgs()) {
            builder.append(getPkgDiagram(pkg));
        }

        for (LinkShapeV2 link : diagram.getLinkSet()) {
            if (link.getDestNode().contains(pkgDefault))
                builder.append(link.getSourceNode()).append("-->").append(link.getDestNode()).append(";").append(System.lineSeparator());
        }

        return  MermaidHelper.formatMermaidScript(builder.toString());

    }

    private String getPkgDiagram(PackageShapeV2 pkg){
        StringBuilder builder = new StringBuilder();

        builder.append("subgraph ").append(pkg.getId()).append("[").append(pkg.getName()).append("]").append(System.lineSeparator());

        for (ShapeV2 cls : pkg.getNodes()) {
            builder.append(cls.getId() + "[" + cls.getName() + "];" + System.lineSeparator());
        }

        for (PackageShapeV2 innerPkg : pkg.getPkgs()) {
            builder.append(getPkgDiagram(innerPkg));
        }

        builder.append("end" + System.lineSeparator());

        return builder.toString();
    }


    // temporary method to accommodate the V3 version
    public String getDiagram(SubGraphDiagramV3 diagram){
        StringBuilder builder = new StringBuilder();
        builder.append("graph LR").append(";").append(System.lineSeparator());

        for (SubGraphShapeV3 pkg : diagram.getSubGraphs()) {
            builder.append(getPkgDiagram(pkg));
        }

        for (LinkShapeV2 link : diagram.getLinkSet()) {
            builder.append(link.getSourceNode()).append("-->").append(link.getDestNode()).append(";").append(System.lineSeparator());
        }

        return  MermaidHelper.formatMermaidScript(builder.toString());

    }

    private String getPkgDiagram(SubGraphShapeV3 pkg){
        StringBuilder builder = new StringBuilder();

        builder.append("subgraph ").append(pkg.getId()).append("[").append(pkg.getName()).append("]").append(System.lineSeparator());

        for (ShapeV2 cls : pkg.getNodes()) {
            builder.append(cls.getId() + "[" + cls.getName() + "];" + System.lineSeparator());
        }

        for (SubGraphShapeV3 innerPkg : pkg.getPkgs()) {
            builder.append(getPkgDiagram(innerPkg));
        }

        builder.append("end" + System.lineSeparator());

        return builder.toString();
    }
}
