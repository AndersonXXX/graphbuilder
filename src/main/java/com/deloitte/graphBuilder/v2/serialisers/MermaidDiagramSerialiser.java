package com.deloitte.graphBuilder.v2.serialisers;

import com.deloitte.graphBuilder.v2.diagrams.DiagramV2;
import com.deloitte.graphBuilder.v2.diagrams.LinkShapeV2;
import com.deloitte.graphBuilder.v2.diagrams.ShapeV2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("MermaidDiagramSerialiser")
public class MermaidDiagramSerialiser implements IDiagramSerialiser {

    // ****************
    // *** IDiagram ***
    // ****************
    public String getDiagram(DiagramV2 diagram){
        StringBuilder builder = new StringBuilder();
        builder.append("graph LR;").append(System.lineSeparator());

//        builder.append("Kafka").append(";").append(System.lineSeparator());
//        builder.append("style Kafka fill:#f9f").append(";").append(System.lineSeparator());

        for (ShapeV2 module : diagram.getShapes()) {
            builder.append(module.getName()).append(";")
                .append(System.lineSeparator());
            if (module.getColor() != null && module.getColor() != "")
                builder.append("style " + module.getName() + " fill:" + module.getColor()).append(";").append(System.lineSeparator());
        }

        for (LinkShapeV2 link : diagram.getLinks()) {
            builder.append(link.getSourceNode()).append("-->|\"").append(link.getName()).append("\"|")
                .append(link.getDestNode()).append(";").append(System.lineSeparator());
        }

        return MermaidHelper.formatMermaidScript(builder.toString());
    }

}
