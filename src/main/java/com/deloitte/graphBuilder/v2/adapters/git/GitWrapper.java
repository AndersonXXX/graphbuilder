package com.deloitte.graphBuilder.v2.adapters.git;

import com.deloitte.graphBuilder.v2.exceptions.GitException;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class GitWrapper {

    private GitConfig config;
    private String decodedPwd;

    private final static String ERROR_CLONE = "Exception occurred while  cloning the repository. Inner Message: {}";
    private final static String ERROR_CHECKOUT = "Exception occurred while Checkout operation on branch {}. Inner Message: {}";
    private final static String ERROR_FETCH_BRANCHES = "Exception occurred while fetching the branches. Inner Message: {}";

    @Autowired
    public GitWrapper(GitConfig config){
        this.config = config;

        // Decode password
        byte[] decodedBytes = Base64.getDecoder().decode(config.getPassword());
        decodedPwd = new String(decodedBytes);
    }

    public void clone(String repoURI, String localRepoPath, String branchName) throws GitException {

        String finalRef = branchName != null ? config.getRepoRef() + branchName : config.getRepoRef() + config.getMainBranch();
        try {
            Git.cloneRepository()
                .setURI(repoURI)
                .setDirectory(new File(localRepoPath))
                .setBranchesToClone(Arrays.asList(finalRef))
                .setBranch(finalRef)
                .setCredentialsProvider( new UsernamePasswordCredentialsProvider(config.getUsername(), decodedPwd))
                .call();
        } catch (GitAPIException e) {
            log.debug(ERROR_CLONE, e.getMessage());
            throw new GitException(ERROR_CLONE + e.getMessage(), e);
        }
    }

    public List<String> getRemoteBranches(String repoURI) throws GitException {

        Collection<Ref> remoteRefs = null;
        try {
            remoteRefs = Git.lsRemoteRepository()
                .setHeads( true )
                .setCredentialsProvider( new UsernamePasswordCredentialsProvider(config.getUsername(), decodedPwd))
                .setRemote( repoURI )
                .call();
        } catch (GitAPIException e) {
            log.debug(ERROR_FETCH_BRANCHES, e.getMessage());
            throw new GitException(ERROR_FETCH_BRANCHES + e.getMessage(), e);
        }

        return remoteRefs.stream().map(Ref::getName).collect(Collectors.toList());
    }

    public List<String> getLocalBranches(String localRepoPath) throws GitException {
        List<String> result = null;
        try {
            result = Git.open(new File(localRepoPath + "/.git")).branchList()
                .call()
                .stream()
                .map(Ref::getName)
                .collect(Collectors.toList());
        } catch (IOException | GitAPIException e) {
            log.debug(ERROR_FETCH_BRANCHES, e.getMessage());
            throw new GitException(ERROR_FETCH_BRANCHES + e.getMessage(), e);
        }
        return result;
    }
}
