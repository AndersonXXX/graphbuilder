package com.deloitte.graphBuilder.v2.adapters.git;

import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix="application.git")
public class GitConfig {

    @NotNull
    private String username;

    @NotNull
    private String password;

    @NotNull
    private String repoRef;

    @NotNull
    private String mainBranch;
}
