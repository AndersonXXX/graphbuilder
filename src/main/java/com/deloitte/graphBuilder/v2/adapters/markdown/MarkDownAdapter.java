package com.deloitte.graphBuilder.v2.adapters.markdown;

import com.deloitte.graphBuilder.v2.adapters.markdown.model.MarkdownLink;
import com.deloitte.graphBuilder.v2.utils.FileUtil;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.Data;

@Data
public class MarkDownAdapter {

    private static final String REGEX_LINK_TO_MD_FULL = "\\[(?<label>.*)\\]\\((?<folder>.*\\/)*(?<filename>.*\\.md?)\\s?(?=\\\"|\\))(?<description>\\\".*\\\")?\\)";

    private String path;
    private String relativePath;
    private String relativeFolder;
    private File raw;
    private List<MarkdownLink> allMatches;

    public MarkDownAdapter(File f) throws IOException {
        this.raw = f;
        this.path = f.getAbsolutePath();
        relativePath = path.substring(FileUtil.getRelativePath().length());
        relativeFolder = relativePath.substring(0, relativePath.lastIndexOf(File.separator));
        allMatches = new ArrayList<>();

        init();
    }

    private void init() throws IOException {
        Path filePath = Path.of(path);
        String content = Files.readString(filePath);

        // Scan for link to other DM files
        Matcher m = Pattern.compile(REGEX_LINK_TO_MD_FULL).matcher(content);
        while (m.find())
            allMatches.add(new MarkdownLink(m.group(1), m.group(2), m.group(3), m.group(4)));
    }

    // ************************
    // *** Public Interface ***
    // ************************
    public List<MarkdownLink> getLinksMD() throws IOException {
        return allMatches;
    }

    public String getRealPath(MarkdownLink linkMD){

        // same folder
        // graphbuilder/documentation/tech/PRChecklist.md-->Model.md;
        if ( !linkMD.getFolder().contains("/"))
            return relativeFolder + "/" + linkMD.getFileName();

        // Link to ancestor folder
        // file.RelativePath   -->   link.Folder + link.FileName (where link.Folder contains or the folder, or a series of ../../ )
        // graphbuilder/documentation/tech/PRChecklist.md-->../ChangeLog.md;
        if( linkMD.getFolder().contains("../")){
            String res = relativeFolder;
            String[] list = linkMD.getFolder().split("/");
            for ( String s : list) {
                res = res.substring(0, res.lastIndexOf("/")); // remove only the slash
            }

            return res + "/" + linkMD.getFileName();
        }
        else
        // from ancestor to sub folder
        // graphbuilder/README.md-->documentation/MermaidVersionScripts.md;
        {
            return relativeFolder + "/" + linkMD.getFolder() + linkMD.getFileName();
        }

    }
}
