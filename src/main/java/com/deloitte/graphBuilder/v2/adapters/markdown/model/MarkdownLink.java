package com.deloitte.graphBuilder.v2.adapters.markdown.model;

import lombok.Data;

@Data
public class MarkdownLink {

    private String label, folder, fileName, description;

    public MarkdownLink(String label, String folder, String fileName, String description){
        this.label = label == null ? "" : label;
        this.folder = folder == null ? "" : folder;
        this.fileName = fileName == null ? "" : fileName;
        this.description = description == null ? "" : description;
    }
}
