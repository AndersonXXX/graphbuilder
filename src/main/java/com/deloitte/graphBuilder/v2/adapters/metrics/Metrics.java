package com.deloitte.graphBuilder.v2.adapters.metrics;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Metrics {

    // ****************
    // *** Business ***
    // ****************
    private static final String MODULE_DIAGRAM_V2_FETCH_SUCCESS = "modulediagramv2.fetch.success";
    private static final String MODULE_DIAGRAM_V2_FETCH_FAIL = "modulediagramv2.fetch.fail";
    private static final String MARKDOWN_DIAGRAM_V2_FETCH_SUCCESS = "markdowndiagram.fetch.success";
    private static final String MARKDOWN_DIAGRAM_V2_FETCH_FAIL = "markdowndiagram.fetch.fail";

    // *****************
    // *** Technical ***
    // *****************

    @Autowired
    private MeterRegistry registry;

    private Counter modulediagramv2FetchSuccess;
    private Counter modulediagramv2FetchFail;
    private Counter markdownDiagramv2FetchSuccess;
    private Counter markdownDiagramv2FetchFail;

    @PostConstruct
    public void initializeCounter() {
        modulediagramv2FetchSuccess = getCounterInstance(MODULE_DIAGRAM_V2_FETCH_SUCCESS);
        modulediagramv2FetchFail = getCounterInstance(MODULE_DIAGRAM_V2_FETCH_FAIL);
        markdownDiagramv2FetchSuccess = getCounterInstance(MARKDOWN_DIAGRAM_V2_FETCH_SUCCESS);
        markdownDiagramv2FetchFail = getCounterInstance(MARKDOWN_DIAGRAM_V2_FETCH_FAIL);
    }

    // ********************************
    // *** Public Counter interface ***
    // ********************************
    public void incrementModuleDiagramV2Succeded() {
        modulediagramv2FetchSuccess.increment();
    }

    public void incrementModuleDiagramV2Fail() {
        modulediagramv2FetchFail.increment();
    }

    public void incrementMarkDownDiagramV2Succeded() {
        markdownDiagramv2FetchSuccess.increment();
    }

    public void incrementMarkDownDiagramV2Fail() {
        markdownDiagramv2FetchFail.increment();
    }

    // Private methods
    private Counter getCounterInstance(String name) {
        return Counter
            .builder(name)
            .register(registry);
    }


}
