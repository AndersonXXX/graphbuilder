package com.deloitte.graphBuilder.v2.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class FileUtil {

    public static boolean exist(String path){
        File f = new File(path);
        return f.exists();
    }

    public static String getBasePath(){
        return System.getProperty("java.io.tmpdir");
    }

    public static String getRelativePath(){
        return getBasePath() + "Waterfall/workspace/";
    }
    public static List<File> getMDlist(String path) throws IOException {
        return  Files.walk(Paths.get(path))
            .filter(Files::isRegularFile)
            .filter(p -> p.toString().endsWith(".md"))
            .map(Path::toFile)
            .collect(Collectors.toList());
    }

    public static List<String> getMDlistAsString(String path) throws IOException {
        return  Files.walk(Paths.get(path))
            .filter(Files::isRegularFile)
            .filter(p -> p.toString().endsWith(".md"))
            .map(Path::toString)
            .collect(Collectors.toList());
    }
}
