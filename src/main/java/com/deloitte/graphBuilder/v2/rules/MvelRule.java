package com.deloitte.graphBuilder.v2.rules;

import com.deloitte.graphBuilder.v2.exceptions.MVELParsingException;
import java.io.StringReader;
import org.jeasy.rules.api.Rule;
import org.jeasy.rules.mvel.MVELRuleFactory;
import org.jeasy.rules.support.reader.YamlRuleDefinitionReader;

public class MvelRule {

    public static Rule createRule(String rule) throws MVELParsingException {
        MVELRuleFactory ruleFactory = new MVELRuleFactory(new YamlRuleDefinitionReader());
        try {
            Rule mvelRule = ruleFactory.createRule(new StringReader(rule));
            return mvelRule;
        } catch (Exception e) {
            throw new MVELParsingException("Error in creation of rule while parsing the string. MSG: " + e.getMessage(), e);
        }
    }

}
