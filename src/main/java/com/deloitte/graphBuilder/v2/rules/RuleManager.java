package com.deloitte.graphBuilder.v2.rules;

import com.deloitte.graphBuilder.v2.exceptions.MVELParsingException;
import com.deloitte.graphBuilder.v2.mveldiagram.MVELPayload;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.StringReader;
import java.util.List;
import org.jeasy.rules.api.Fact;
import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rule;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;
import org.jeasy.rules.mvel.MVELRuleFactory;
import org.jeasy.rules.support.reader.YamlRuleDefinitionReader;
import org.springframework.beans.factory.annotation.Autowired;

public class RuleManager {

    RulesEngine engine;
    Rules rules;
    Facts facts;

    @Autowired
    public RuleManager() {
        engine = new DefaultRulesEngine();
        rules = new Rules();
        facts = new Facts();
    }


    // **********************
    // *** Public Methods ***
    // **********************
    public void addRules(List<MVELPayload> inputRules) throws MVELParsingException {
        for (MVELPayload aRule : inputRules) {
            addRuleJSON(aRule);
        }
    }

    public void addRuleJSON(MVELPayload inputRule) throws MVELParsingException {
        ObjectMapper mapper = new ObjectMapper();
        String mvelRuleText = null;
        try {
            mvelRuleText = mapper.writeValueAsString(inputRule);

            MVELRuleFactory ruleFactory = new MVELRuleFactory(new YamlRuleDefinitionReader());
            Rule mvelRule = ruleFactory.createRule(new StringReader(mvelRuleText));

            rules.register(mvelRule);
        } catch (JsonProcessingException e) {
            throw new MVELParsingException("Error in mapping inputRule into JSON string. MSG: " + e.getMessage(), e);
        } catch (Exception e) {
            throw new MVELParsingException("Error in creation of rule while parsing the string. MSG: " + e.getMessage(), e);
        }

    }

    public void addFact(String key, Object value){
        Fact fact = new Fact(key, value);
        facts.add(fact);
    }

    public void fire(){
        engine.fire(rules, facts);
    }

}
