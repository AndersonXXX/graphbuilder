package com.deloitte.graphBuilder.v2.readers;

import com.deloitte.graphBuilder.AppConfig;
import com.deloitte.graphBuilder.v2.exceptions.GitException;
import com.deloitte.graphBuilder.v2.exceptions.QDOXParsingException;
import com.deloitte.graphBuilder.v2.adapters.git.GitWrapper;
import com.deloitte.graphBuilder.v2.utils.FileUtil;
import com.thoughtworks.qdox.JavaProjectBuilder;
import com.thoughtworks.qdox.model.JavaSource;
import java.io.File;
import java.util.Collection;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class JavaSourceReader {

    private GitWrapper wrapper;
    private AppConfig config;

    @Autowired
    public JavaSourceReader(AppConfig config, GitWrapper wrapper){
        this.config = config;
        this.wrapper = wrapper;
    }

    // parameterPath is coming from UI
    public Collection<JavaSource> readRepoURI(String repoURI, String rootPath, String branchName) throws QDOXParsingException, GitException {

        String projName = repoURI.substring(repoURI.lastIndexOf("/")+1, repoURI.indexOf(".git"));
        String sourcePath = FileUtil.getBasePath() + config.getRelativePath() + projName;

        if( !FileUtil.exist(sourcePath) ) {
            try {
                // call Git
                wrapper.clone(repoURI, sourcePath, branchName);
            } catch (GitException e) {
                log.error("exception occurred while cloning the repository. Inner messge: {}", e.getMessage());
                throw e;
            }
        } else if (FileUtil.exist(sourcePath + "/.git")){
//            wrapper.checkout(sourcePath, branchName);
        }

        // at this point sourcePath has the pat to the workspace plus the name of the project. We need to add the rootPath set
        // from the user so the reader can parse only a subpackage of the application
        String readPath = sourcePath + rootPath;

        return read(readPath);
    }

    public Collection<JavaSource> read(String sourcePath) throws QDOXParsingException{

        // Prepare the reader
        JavaProjectBuilder builder = new JavaProjectBuilder();
        try {
            builder.addSourceTree(new File(sourcePath));
        } catch (Exception e) {
            throw new QDOXParsingException("Error in creation of rule while parsing the string. MSG: " + e.getMessage(), e);
        }
        return builder.getSources();
    }

}
