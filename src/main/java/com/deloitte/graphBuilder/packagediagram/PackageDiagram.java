package com.deloitte.graphBuilder.packagediagram;

import com.deloitte.graphBuilder.model.ClassShape;
import com.deloitte.graphBuilder.model.LinkShape;
import com.deloitte.graphBuilder.model.PackageShape;
import com.thoughtworks.qdox.model.JavaSource;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PackageDiagram{

    private List<PackageShape> pkgs = new ArrayList<>();
    private List<LinkShape> links = new ArrayList<>();

    public void addPackage(String pkg){
        String pkgName = extractPackage(pkg);

        PackageShape pkgShape;
        if (pkgs.stream().filter(p -> p.getName().equals(pkgName)).findFirst().isPresent()) {
            pkgShape = pkgs.stream().filter(p -> p.getName().equals(pkgName)).findFirst().get();
        }
        else{
            pkgShape = PackageShape.builder()
                .pkgs(new ArrayList<>())
                .classes(new ArrayList<>())
                .name(pkgName)
                .id(UUID.randomUUID().toString()).build();
            pkgs.add(pkgShape);
        }

        if (pkg.contains("."))
            pkgShape.addPackage(pkg.substring(pkg.indexOf(".") + 1));
    }

    public void addClass(JavaSource src){
        String pkgName = extractPackage(src.getPackageName());
        String clasName = src.getClasses().stream().findFirst().get().getName();

        pkgs.stream().filter(p -> p.getName().equals(pkgName)).findFirst().get()
                .addClassInPackage(new ClassShape(clasName), src.getPackageName().substring(src.getPackageName().indexOf(".")+1));
    }

    private String extractPackage(String pkg){
        if(pkg.contains("."))
            return pkg.substring(0, pkg.indexOf("."));
        else
            return pkg;
    }

}
