package com.deloitte.graphBuilder.packagediagram;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/package")
@RequiredArgsConstructor
@Api(tags = "Package")
@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
public class PackageDiagramController {

    private final PackageDiagramProcessor processor;

    @ApiOperation(value = "Endpoint to generate package diagram for all the application POST")
    @PostMapping("/getAllPackageDiagramPOST")
    public String getAllPackageDiagramPOST(@RequestBody @Valid InPayload params){
        System.out.println("Call received - getAllPackageDiagram");
        processor.process(params);
        return processor.getDiagram();

    }

}
