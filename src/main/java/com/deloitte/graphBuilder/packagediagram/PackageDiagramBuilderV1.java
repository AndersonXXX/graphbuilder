package com.deloitte.graphBuilder.packagediagram;

import java.util.HashSet;
import java.util.Set;

public class PackageDiagramBuilderV1 {
    Set<String> set = new HashSet<>();

    public void addLink(String source, String dest){
        set.add(source + "-->" + dest + ";</br>");
    }

    @Override
    public String toString(){
        StringBuilder graph = new StringBuilder();
        graph.append("graph TD;").append(System.getProperty("line.separator"));
        for (String a : set) {
            graph.append(a);
        }
        return graph.toString();
    }
}
