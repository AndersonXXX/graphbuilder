package com.deloitte.graphBuilder.packagediagram;

import com.deloitte.graphBuilder.model.ClassShape;
import com.deloitte.graphBuilder.model.LinkShape;
import com.deloitte.graphBuilder.model.PackageShape;
import com.deloitte.graphBuilder.service.IBuilder;
import com.deloitte.graphBuilder.service.IDiagram;
import com.thoughtworks.qdox.model.JavaPackage;
import com.thoughtworks.qdox.model.JavaSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("PackageDiagramBuilderV2")
public class PackageDiagramBuilderV2 implements IBuilder, IDiagram {

    private PackageDiagram diagram = new PackageDiagram();

    public void init(InPayload params){
        diagram = new PackageDiagram();
    }

    public String getDiagram(){
        StringBuilder builder = new StringBuilder();
        builder.append("graph LR").append(";").append(System.lineSeparator());

        for (PackageShape pkg : diagram.getPkgs()) {
            builder.append(getPkgDiagram(pkg));
        }

        for (LinkShape link : diagram.getLinks()) {
            builder.append(link.getName());
        }

        return builder.toString();

    }

    private String getPkgDiagram(PackageShape pkg){
        StringBuilder builder = new StringBuilder();

        builder.append("subgraph ").append(pkg.getId() + "[" +  pkg.getName() + "];").append(System.lineSeparator());

        for (PackageShape innerPkg : pkg.getPkgs()) {
            builder.append(getPkgDiagram(innerPkg));
        }

        for (ClassShape cls : pkg.getClasses()) {
            builder.append(cls.getId() + "[" + cls.getName() + "];" + System.lineSeparator());
        }
        builder.append("end" + System.lineSeparator());

        return builder.toString();
    }

    public void addPackage(JavaPackage pkg){
        diagram.addPackage(pkg.getName());
    }

    public void addPackage(String pkgName){
        diagram.addPackage(pkgName);
    }

    public void addClass(JavaSource src){
        diagram.addClass(src);
    }

}
