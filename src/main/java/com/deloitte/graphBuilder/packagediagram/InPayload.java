package com.deloitte.graphBuilder.packagediagram;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class InPayload {
    @NotNull
    @ApiModelProperty(example = "/Users/mdellarovere/Projects/workspace/graphBuilder/src/main/java/com/deloitte/graphBuilder/v2")
    private String sourcePath;

    @NotNull
    @ApiModelProperty(example = "com.deloitte.graphBuilder")
    String pkgDefault;

    @NotNull
    @ApiModelProperty(example = "graphBuilder")
    String projName;

    boolean deep, wide;
}
