package com.deloitte.graphBuilder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class GraphBuilderApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraphBuilderApplication.class, args);
	}

}
