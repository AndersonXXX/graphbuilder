package com.deloitte.graphBuilder.model;

import lombok.Data;

@Data
public class Shape {
    private int position;
    private String id, name;

    public Shape(int position, String name){
        this.position = position;
        this.id = name;
        this.name = name;
    }

    public Shape(int position, String id, String name){
        this.position = position;
        this.id = id;
        this.name = name;
    }
}
