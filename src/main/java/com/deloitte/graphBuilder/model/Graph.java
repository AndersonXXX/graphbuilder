package com.deloitte.graphBuilder.model;


public class Graph {

    private StringBuilder graph = new StringBuilder();
    private StringBuilder links = new StringBuilder();

    public void addLink(String source, String dest){
        links.append(source).append("-->").append(dest).append(";").append(System.getProperty("line.separator"));
    }

    @Override
    public String toString(){
        graph.append("graph LR;").append(System.getProperty("line.separator"));
        graph.append(links.toString());
        return graph.toString();
    }
}
