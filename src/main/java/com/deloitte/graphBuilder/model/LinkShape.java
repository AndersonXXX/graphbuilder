package com.deloitte.graphBuilder.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

enum Type {
    FULL_LINE,
    FULL_LINE_ARROW,
    DOT_LINE_ARROW,
    THICK_LINE_ARROW
}

@Data
@EqualsAndHashCode(callSuper=true)
public class LinkShape extends Shape{

    private String sourceNode, destNode;
    private Type type;

    @Builder
    public LinkShape(String sourceNode, String destNode, String name){
        super(0, "id", name);
        this.sourceNode = sourceNode;
        this.destNode = destNode;
    }

    @Builder
    public LinkShape(String id, String sourceNode, String destNode, String name){
        super(0, id, name);
        this.sourceNode = sourceNode;
        this.destNode = destNode;
    }


}
