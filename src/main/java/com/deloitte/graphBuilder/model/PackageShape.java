package com.deloitte.graphBuilder.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class PackageShape extends Shape{

    private List<PackageShape> pkgs;
    private List<ClassShape> classes;

    @Builder
    public PackageShape(List<PackageShape> pkgs, List<ClassShape> classes, String id, String name){
        super(0, id, name);
        this.pkgs = pkgs;
        this.classes = classes;
    }

    public void addPackage(String pkg){
        String pkgName = extractPackage(pkg);

        PackageShape pkgShape;
        if (pkgs.stream().filter(p -> p.getName().equals(pkgName)).findFirst().isPresent()) {
            pkgShape = pkgs.stream().filter(p -> p.getName().equals(pkgName)).findFirst().get();
        }
        else{
            pkgShape = PackageShape.builder()
                .pkgs(new ArrayList<>())
                .classes(new ArrayList<>())
                .name(pkgName)
                .id(UUID.randomUUID().toString()).build();
            pkgShape.getClasses().add(new ClassShape(UUID.randomUUID().toString(),pkgName));
            pkgs.add(pkgShape);
        }

        if (pkg.contains("."))
            pkgShape.addPackage(pkg.substring(pkg.indexOf(".") + 1));
    }

    //this method create packages with the id equal to the previous package part
    // i.e. if arrive com.deloitte.model the package model will have the name as "model" but the id as "com.deloitte.model"
    public void addPackageWithSource(String pkg, String sourcePkg){
        String pkgName = extractPackage(pkg);

        PackageShape pkgShape;
        if (pkgs.stream().filter(p -> p.getName().equals(pkgName)).findFirst().isPresent()) {
            pkgShape = pkgs.stream().filter(p -> p.getName().equals(pkgName)).findFirst().get();
        }
        else{
            pkgShape = PackageShape.builder()
                .pkgs(new ArrayList<>())
                .classes(new ArrayList<>())
                .name(pkgName)
                .id(UUID.randomUUID().toString()).build();
            pkgShape.getClasses().add(new ClassShape(sourcePkg+"."+pkgName, pkgName));
            pkgs.add(pkgShape);
        }

        if (pkg.contains("."))
            pkgShape.addPackageWithSource(pkg.substring(pkg.indexOf(".") + 1), sourcePkg+"."+pkgName);
    }

    public void addClassInPackage(ClassShape clsShape, String pkg){
        String pkgName = extractPackage(pkg);
        if (pkg.contains("."))
            pkgs.stream().filter(p -> p.getName().equals(pkgName)).findFirst().get()
                    .addClassInPackage(clsShape, pkg.substring(pkg.indexOf(".")+1));
        else
            pkgs.stream().filter(p -> p.getName().equals(pkgName)).findFirst().get()
                    .addClass(clsShape);
    }

    private void addClass(ClassShape clsShape){
        this.classes.add(clsShape);
    }

    private String extractPackage(String pkg){
        if(pkg.contains("."))
            return pkg.substring(0, pkg.indexOf("."));
        else
            return pkg;
    }

}
