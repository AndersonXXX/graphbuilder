package com.deloitte.graphBuilder.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class ClassShape extends Shape{

    @Builder
    public ClassShape(String name){
        super(0, "id", name);
    }

    @Builder
    public ClassShape(String id, String name){
        super(0, id, name);
    }

}
