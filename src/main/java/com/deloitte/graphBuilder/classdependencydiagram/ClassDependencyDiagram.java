package com.deloitte.graphBuilder.classdependencydiagram;

import com.deloitte.graphBuilder.model.LinkShape;
import com.deloitte.graphBuilder.model.Shape;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClassDependencyDiagram {
    private List<Shape> shapes = new ArrayList<>();
    private List<LinkShape> links = new ArrayList<>();

}
