package com.deloitte.graphBuilder.classdependencydiagram;

import com.deloitte.graphBuilder.packagediagram.InPayload;
import com.deloitte.graphBuilder.service.IBuilder;
import com.deloitte.graphBuilder.service.IDiagram;
import com.thoughtworks.qdox.JavaProjectBuilder;
import com.thoughtworks.qdox.model.JavaSource;
import java.io.File;
import java.io.IOException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ClassDependencyDiagramProcessor {

    private final IBuilder builder;
    private InPayload params;

    @Autowired
    public ClassDependencyDiagramProcessor(@Qualifier("ClassDependencyDiagramBuilder") IBuilder builder, InPayload params) {
        this.builder = builder;
        this.params = params;
    }

    public String getDiagram(){
        return ((IDiagram)builder).getDiagram();
    }

    public void process(InPayload params) throws IOException {
        this.params=params;
        this.builder.init(params);

        // Prepare the reader
        JavaProjectBuilder builder = new JavaProjectBuilder();
        JavaSource src = builder.addSource(new File(params.getSourcePath()));

        this.builder.addClass(src);
    }

}
