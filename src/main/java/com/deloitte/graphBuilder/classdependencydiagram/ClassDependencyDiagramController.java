package com.deloitte.graphBuilder.classdependencydiagram;

import com.deloitte.graphBuilder.packagediagram.InPayload;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.IOException;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/classdependency")
@RequiredArgsConstructor
@Api(tags = "Class Dependency")
@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
public class ClassDependencyDiagramController {

    private final ClassDependencyDiagramProcessor processor;

    @ApiOperation(value = "Endpoint to generate the class dependency diagram of one class")
    @PostMapping("/getClassDependencyDiagram")
    public String getClassDependencyDiagram(@RequestBody @Valid InPayload params){
        System.out.println("Call received - getClassDependencyDiagram");
        try {
            processor.process(params);
        } catch (IOException e) {
            return e.getMessage();
        }
        return processor.getDiagram();

    }
}
