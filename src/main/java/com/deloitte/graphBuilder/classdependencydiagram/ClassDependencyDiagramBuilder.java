package com.deloitte.graphBuilder.classdependencydiagram;

import com.deloitte.graphBuilder.model.LinkShape;
import com.deloitte.graphBuilder.model.Shape;
import com.deloitte.graphBuilder.packagediagram.InPayload;
import com.deloitte.graphBuilder.service.IBuilder;
import com.deloitte.graphBuilder.service.IDiagram;
import com.thoughtworks.qdox.model.JavaPackage;
import com.thoughtworks.qdox.model.JavaSource;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("ClassDependencyDiagramBuilder")
public class ClassDependencyDiagramBuilder implements IBuilder, IDiagram {

    private ClassDependencyDiagram diagram = new ClassDependencyDiagram();
    private String pkgDefault;

    public void init(InPayload params){
        diagram = new ClassDependencyDiagram();
        this.pkgDefault = params.getPkgDefault();
    }


    //************************************************************
    // ****************
    // *** IBuilder ***
    // ****************
    public void addPackage(JavaPackage pkg){

    }

    public void addPackage(String pkgName){

    }

    public void addClass(JavaSource src){
        String clasName = src.getClasses().stream().findFirst().get().getName();

        //Create the shape and add it to the list of Shapes
        diagram.getShapes().add(new Shape(0, clasName));

        // create the links for each import in the class that match the package of the app (we don't show dependency with JDK)
        List<String> imports = src.getImports();
        for (String imp : imports) {
            if (imp.contains(pkgDefault)){
                diagram.getShapes().add(new Shape(0, imp.substring(imp.lastIndexOf(".")+1)));
                diagram.getLinks().add(LinkShape.builder()
                    .name("")
                    .sourceNode(clasName)
                    .destNode(imp.substring(imp.lastIndexOf(".")+1))
                    .build());
            }

        }
    }
    //************************************************************

    // ****************
    // *** IDiagram ***
    // ****************
    public String getDiagram(){
        StringBuilder builder = new StringBuilder();
        builder.append("graph LR;").append(System.lineSeparator());

        for (Shape module : diagram.getShapes()) {
            builder.append(module.getName()).append(";")
                    .append(System.lineSeparator());
        }

        for (LinkShape link : diagram.getLinks()) {
            builder.append(link.getSourceNode()).append("-->|\"").append(link.getName()).append("\"|")
                    .append(link.getDestNode()).append(";").append(System.lineSeparator());
        }

        return builder.toString();
    }

}
