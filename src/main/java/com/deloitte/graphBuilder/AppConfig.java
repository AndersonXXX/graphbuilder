package com.deloitte.graphBuilder;

import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix="application")
public class AppConfig {

    @NotNull
    private String workspacePath;

    @NotNull
    private String relativePath;

}