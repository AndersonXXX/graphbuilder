package com.deloitte.graphBuilder.modulediagram;

import com.deloitte.graphBuilder.model.LinkShape;
import com.deloitte.graphBuilder.model.Shape;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ModuleDiagram{
    private Shape mainModule;
    private List<Shape> shapes = new ArrayList<>();
    private List<LinkShape> links = new ArrayList<>();

}
