package com.deloitte.graphBuilder.modulediagram;

import com.deloitte.graphBuilder.packagediagram.InPayload;
import com.deloitte.graphBuilder.service.IBuilder;
import com.deloitte.graphBuilder.service.IDiagram;
import com.thoughtworks.qdox.JavaProjectBuilder;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaSource;
import java.io.File;
import java.util.Iterator;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ModuleDiagramProcessor {

    private final IBuilder builder;
    private InPayload params;

    @Autowired
    public ModuleDiagramProcessor(@Qualifier("ModuleDiagramBuilder") IBuilder builder, InPayload params) {
        this.builder = builder;
        this.params = params;
    }

    public String getDiagram(){
        return ((IDiagram)builder).getDiagram();
    }

    public void process(InPayload params){
        this.params=params;
        builder.init(params);

        // Prepare the reader
        JavaProjectBuilder builder = new JavaProjectBuilder();
        builder.addSourceTree(new File(params.getSourcePath()));

        // Iterate through the file (classes source code) and construct the output
        Iterator iterCls = builder.getSources().iterator();
        processClasses(iterCls);
    }

    private void processClasses(Iterator iter){
        while (iter.hasNext()) {

            // get one source file xyz.java
            JavaClass cls = ((JavaSource) iter.next()).getClasses().stream().findFirst().get();
            builder.addClass(cls.getSource());

//            Old implementation
//            //@org.springframework.cloud.openfeign.FeignClient(name="threat-metrix-adapter",url="${tmx.url}",configuration=ThreatMetrixConfig.class)
//            if (cls.getPackageName().contains(params.getPkgDefault()) && name.contains("Client") && isInterface)
//                builder.addClass(cls.getSource());

        }
    }

}
