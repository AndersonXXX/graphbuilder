package com.deloitte.graphBuilder.modulediagram;

import com.deloitte.graphBuilder.model.LinkShape;
import com.deloitte.graphBuilder.model.Shape;
import com.deloitte.graphBuilder.packagediagram.InPayload;
import com.deloitte.graphBuilder.service.IBuilder;
import com.deloitte.graphBuilder.service.IDiagram;
import com.thoughtworks.qdox.model.JavaAnnotation;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaField;
import com.thoughtworks.qdox.model.JavaMethod;
import com.thoughtworks.qdox.model.JavaPackage;
import com.thoughtworks.qdox.model.JavaSource;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("ModuleDiagramBuilder")
public class ModuleDiagramBuilder implements IBuilder, IDiagram {

    private ModuleDiagram diagram = new ModuleDiagram();

    public void init(InPayload params){
        diagram = new ModuleDiagram();
        diagram.setMainModule(new Shape(0, params.getProjName()));
    }

    // ****************
    // *** IDiagram ***
    // ****************
    public String getDiagram(){
        StringBuilder builder = new StringBuilder();
        builder.append("graph LR;").append(System.lineSeparator());

        builder.append(diagram.getMainModule().getName())
            .append(";").append(System.lineSeparator());

        builder.append("Kafka").append(";").append(System.lineSeparator());
        builder.append("style Kafka fill:#f9f").append(";").append(System.lineSeparator());

        for (Shape module : diagram.getShapes()) {
            builder.append(module.getName()).append(";")
                .append(System.lineSeparator());
        }

        for (LinkShape link : diagram.getLinks()) {
            builder.append(link.getSourceNode()).append("-->|\"").append(link.getName()).append("\"|")
                .append(link.getDestNode()).append(";").append(System.lineSeparator());
        }

        return builder.toString();
    }


    //************************************************************
    // ****************
    // *** IBuilder ***
    // ****************
    public void addPackage(JavaPackage pkg){

    }

    public void addPackage(String pkgName){

    }

    public void addClass(JavaSource src) {

        JavaClass cls = src.getClasses().stream().findFirst().get();

        // Clients
        if (cls.getSource().getImports().contains("org.springframework.cloud.openfeign.FeignClient")) {
            processForClient(src);
        }

        if (cls.getSource().getImports().contains("org.springframework.kafka.core.KafkaTemplate")) {
            processForKafkaProducer(src.getClasses().stream().findFirst().get());
        }

        if (cls.getSource().getImports().contains("org.springframework.kafka.annotation.KafkaListener")) {
            processForKafkaListener(src.getClasses().stream().findFirst().get());
        }
    }

    private void processForKafkaProducer(JavaClass cls){

        //check if it is a Producer
        if (cls.getSource().getImports().contains("org.springframework.kafka.core.KafkaTemplate")){
            String topicMappingName = "";
            List<JavaField> fields = cls.getFields();

            // foreach field, check if it has the annotation for the Kafka topic. The annotaiton value is the key
            // to be used in the application.yml to get the name of the topic
            for (JavaField field : fields) {
                JavaAnnotation ann = field.getAnnotations().stream()
                    .filter(javaAnnotation -> javaAnnotation.getType().getName().equals("Value")).findFirst().orElse(null);

                if (ann != null) {
                    topicMappingName = (String) ann.getProperty("value").getParameterValue();
                    topicMappingName = removeSpecialCharacter(topicMappingName);

                    diagram.getLinks().add(LinkShape.builder()
                        .name(topicMappingName)
                        .sourceNode(diagram.getMainModule().getName())
                        .destNode("Kafka")
                        .build());
                }
            }
        }
    }

    private void processForKafkaListener(JavaClass cls){
        //check if it is a Listener
        if (cls.getSource().getImports().contains("org.springframework.kafka.annotation.KafkaListener")){
            List<JavaMethod> methods = cls.getMethods();

            // foreach method, check if it has the annotation for the Kafka topic. The annotation value is the key
            // to be used in the application.yml to get the name of the topic
            for (JavaMethod method : methods) {
                JavaAnnotation ann = method.getAnnotations().stream()
                    .filter(javaAnnotation -> javaAnnotation.getType().getName().equals("org.springframework.kafka.annotation.KafkaListener")).findFirst().orElse(null);

                if (ann != null) {
                    String topicMappingName = "";
                    if ( ann.getProperty("topics") != null)
                        topicMappingName = (String) ann.getProperty("topics").getParameterValue();
                    topicMappingName = removeSpecialCharacter(topicMappingName);

                    String groupId = "";
                    if (ann.getProperty("groupId") != null)
                        groupId = (String) ann.getProperty("groupId").getParameterValue();
                    groupId = removeSpecialCharacter(groupId);

                    String linkText = topicMappingName != "" ? "Topic: "+topicMappingName : "";
                    linkText += groupId != "" ? " - GroupId: " + groupId : "";

                    diagram.getLinks().add(LinkShape.builder()
                        .name(linkText)
                        .sourceNode("Kafka")
                        .destNode(diagram.getMainModule().getName())
                        .build());
                }
            }
        }
    }


    public void processForClient(JavaSource src){

        String clasName = src.getClasses().stream().findFirst().get().getName();

        //Create the shape and add it to the list of Shapes
        diagram.getShapes().add(new Shape(0, clasName.substring(0, clasName.indexOf("Client"))));

        // create the links for each method in the class
        JavaClass cls = src.getClasses().stream().findFirst().get();
        List<JavaMethod> methods = cls.getMethods();
        for (JavaMethod method : methods) {
            diagram.getLinks().add(LinkShape.builder()
                .name(method.getCallSignature())
                .sourceNode(diagram.getMainModule().getName())
                .destNode(clasName.substring(0, clasName.indexOf("Client")))
                .build());
        }
    }

    private String removeSpecialCharacter(String toClean){
        String ret = toClean;
        ret = ret.replace("\"", "");
        ret = ret.replace("$", "");

        return ret;
    }
}
