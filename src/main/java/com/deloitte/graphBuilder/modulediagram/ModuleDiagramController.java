package com.deloitte.graphBuilder.modulediagram;

import com.deloitte.graphBuilder.packagediagram.InPayload;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/api/module")
@RequiredArgsConstructor
@Api(tags = "Module")
public class ModuleDiagramController {

    private final ModuleDiagramProcessor processor;

    @ApiOperation(value = "Endpoint to generate the module diagram of the application")
    @PostMapping("/getModuleDiagram")
    public String getModuleDiagram(@RequestBody @Valid InPayload params){
        System.out.println("Call received - getModuleDiagram");
        processor.process(params);
        return processor.getDiagram();

    }
}
