package com.deloitte.graphBuilder.packagedependencydiagram;

import com.deloitte.graphBuilder.packagediagram.InPayload;
import com.deloitte.graphBuilder.service.IBuilder;
import com.deloitte.graphBuilder.service.IDiagram;
import com.thoughtworks.qdox.JavaProjectBuilder;
import com.thoughtworks.qdox.model.JavaPackage;
import com.thoughtworks.qdox.model.JavaSource;
import java.io.File;
import java.util.Iterator;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PackageDependencyDiagramProcessor {

    private final IBuilder builder;
    private InPayload params;

    @Autowired
    public PackageDependencyDiagramProcessor(@Qualifier("PackageDependencyDiagramBuilder") IBuilder builder, InPayload params) {
        this.builder = builder;
        this.params = params;
    }

    public String getDiagram(){
        return ((IDiagram)builder).getDiagram();
    }

    public void process(InPayload params){
        this.params=params;
        builder.init(params);

        // Prepare the reader
        JavaProjectBuilder builder = new JavaProjectBuilder();
        builder.addSourceTree(new File(params.getSourcePath()));

        // iterate through the packages
//        Iterator iterPkg = builder.getPackages().iterator();
//        processPackages(iterPkg);

        // Iterate through the file (classes source code) and construct the output
        Iterator iterCls = builder.getSources().iterator();
        processClasses(iterCls);
    }

    private void processPackages(Iterator iter){
        while (iter.hasNext()) {
            // get one source file xyz.java
            JavaPackage pkg = (JavaPackage) iter.next();
            System.out.println("Parsing package: " + pkg.getName());

            if (pkg.getName().contains(params.getPkgDefault())){
                builder.addPackage(pkg);
            }

        }
    }

    private void processClasses(Iterator iter){
        while (iter.hasNext()) {

            // get one source file xyz.java
            JavaSource src = (JavaSource) iter.next();

            if (src.getPackageName().contains(params.getPkgDefault())){
                builder.addPackage(src.getPackageName());
            }

            if (src.getPackageName().contains(params.getPkgDefault()))
                builder.addClass(src);

        }
    }
}

