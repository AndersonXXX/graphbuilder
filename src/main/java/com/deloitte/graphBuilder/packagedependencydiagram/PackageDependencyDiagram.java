package com.deloitte.graphBuilder.packagedependencydiagram;

import com.deloitte.graphBuilder.model.ClassShape;
import com.deloitte.graphBuilder.model.LinkShape;
import com.deloitte.graphBuilder.model.PackageShape;
import com.thoughtworks.qdox.model.JavaSource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PackageDependencyDiagram {

    private List<PackageShape> pkgs = new ArrayList<>();
    private List<LinkShape> links = new ArrayList<>();
    private Set<LinkShape> linkSet = new HashSet<>();

    public void addPackage(String pkg){
        String pkgName = extractPackage(pkg);

        PackageShape pkgShape;
        if (pkgs.stream().filter(p -> p.getName().equals(pkgName)).findFirst().isPresent()) {
            pkgShape = pkgs.stream().filter(p -> p.getName().equals(pkgName)).findFirst().get();
        }
        else{
            pkgShape = PackageShape.builder()
                .pkgs(new ArrayList<>())
                .classes(new ArrayList<>())
                .name(pkgName)
                .id(UUID.randomUUID().toString()).build();
            pkgShape.getClasses().add(new ClassShape(pkgName,pkgName));
            pkgs.add(pkgShape);
        }

        if (pkg.contains("."))
            pkgShape.addPackageWithSource(pkg.substring(pkg.indexOf(".") + 1), pkgName);
    }

    public void addClass(JavaSource src){
        String pkgName = src.getPackageName();
        String clasName = src.getClasses().stream().findFirst().get().getName();

        System.out.println("Class: " + src.getClasses().get(0).getName());
        for ( String imp : src.getImports()) {
            linkSet.add(new LinkShape("0", pkgName, imp.substring(0, imp.lastIndexOf('.')), ""));
        }
    }

    private String extractPackage(String pkg){
        if(pkg.contains("."))
            return pkg.substring(0, pkg.indexOf("."));
        else
            return pkg;
    }

}
