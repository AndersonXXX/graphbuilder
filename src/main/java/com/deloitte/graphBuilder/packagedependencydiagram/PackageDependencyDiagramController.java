package com.deloitte.graphBuilder.packagedependencydiagram;

import com.deloitte.graphBuilder.packagediagram.InPayload;
import com.deloitte.graphBuilder.v2.serialisers.MermaidHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/package")
@RequiredArgsConstructor
@Api(tags = "Package")
@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
public class PackageDependencyDiagramController {

    private final PackageDependencyDiagramProcessor processor;

    @ApiOperation(value = "Endpoint to generate package diagram with dependency for all the application POST")
    @PostMapping("/getAllPackageDependencyDiagramPOST")
    public String getAllPackageDependencyDiagramPOST(@RequestBody @Valid InPayload params){
        System.out.println("Call received - getPackageDependencyDiagram");
        processor.process(params);
        return MermaidHelper.formatMermaidScript(processor.getDiagram());

    }

}
