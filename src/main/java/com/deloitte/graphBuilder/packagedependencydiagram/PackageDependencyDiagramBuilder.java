package com.deloitte.graphBuilder.packagedependencydiagram;

import com.deloitte.graphBuilder.model.ClassShape;
import com.deloitte.graphBuilder.model.LinkShape;
import com.deloitte.graphBuilder.model.PackageShape;
import com.deloitte.graphBuilder.packagediagram.InPayload;
import com.deloitte.graphBuilder.service.IBuilder;
import com.deloitte.graphBuilder.service.IDiagram;
import com.thoughtworks.qdox.model.JavaPackage;
import com.thoughtworks.qdox.model.JavaSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("PackageDependencyDiagramBuilder")
public class PackageDependencyDiagramBuilder implements IBuilder, IDiagram {

    private PackageDependencyDiagram diagram = new PackageDependencyDiagram();
    private String pkgDefault;

    public void init(InPayload params){
        diagram = new PackageDependencyDiagram();
        this.pkgDefault = params.getPkgDefault();
    }

    public String getDiagram(){
        StringBuilder builder = new StringBuilder();
        builder.append("graph LR").append(";").append(System.lineSeparator());

        for (PackageShape pkg : diagram.getPkgs()) {
            builder.append(getPkgDiagram(pkg));
        }

        for (LinkShape link : diagram.getLinkSet()) {
            if (link.getDestNode().contains(pkgDefault))
                builder.append(link.getSourceNode()).append("-->").append(link.getDestNode()).append(";").append(System.lineSeparator());
        }

        return builder.toString();

    }

    private String getPkgDiagram(PackageShape pkg){
        StringBuilder builder = new StringBuilder();

        builder.append("subgraph ").append(pkg.getId()).append("[").append(pkg.getName()).append("]").append(System.lineSeparator());

        for (ClassShape cls : pkg.getClasses()) {
            builder.append(cls.getId() + "[" + cls.getName() + "];" + System.lineSeparator());
        }

        for (PackageShape innerPkg : pkg.getPkgs()) {
            builder.append(getPkgDiagram(innerPkg));
        }

        builder.append("end" + System.lineSeparator());

        return builder.toString();
    }

    public void addPackage(JavaPackage pkg){
        diagram.addPackage(pkg.getName());
    }

    public void addPackage(String pkgName){
        diagram.addPackage(pkgName);
    }

    public void addClass(JavaSource src){
        diagram.addClass(src);
    }

}
