package com.deloitte.graphBuilder.controller;

import com.deloitte.graphBuilder.model.Graph;
import com.deloitte.graphBuilder.packagediagram.PackageDiagramBuilderV1;
import com.thoughtworks.qdox.JavaProjectBuilder;
import com.thoughtworks.qdox.model.JavaSource;
import java.io.File;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
public class HelloController {
    @GetMapping("/health")
    public String hello() {
        System.out.println();
        return "Hello, the time at the server is now " + new Date() + "\n";
    }

    @GetMapping("/api/getClassDiagram")
    public String getClassDiagram(){
        return generateScriptForClassDiagram("/Users/mdellarovere/Projects/workspace/transaction-monitoring-adapter/src/main/java/com/tenx/transactionmonitoringadapter");
    }

    @GetMapping("/api/getPackageDiagram")
    public String getPackageDiagram(){
        String pathFAM = "/Users/mdellarovere/Projects/workspace/fraud-aml-manager/src/main/java/com/tenx/fraudamlmanager";
        String pathTMA = "/Users/mdellarovere/Projects/workspace/transaction-monitoring-adapter/src/main/java/com/tenx/transactionmonitoringadapter";
        return generateScriptForPackageDiagram(pathFAM);
    }


    private String generateScriptForClassDiagram(String fileFullPath){

        // Prepare the reader
        //String fileFullPath = "/Users/mdellarovere/Projects/workspace/graphBuilder/src/main/java";
        JavaProjectBuilder builder = new JavaProjectBuilder();
        builder.addSourceTree(new File(fileFullPath));

        // create the object Graph
        Graph graph = new Graph();

        // Iterate through the file (classes source code) and construct the output
        Iterator iter = builder.getSources().iterator();
        while (iter.hasNext()) {

            // get one source file xyz.java
            JavaSource src = (JavaSource) iter.next();

            List<String> imports = src.getImports();
            String clasName = src.getClasses().stream().findFirst().orElse(null).getName();
            System.out.println("Import for class: " + clasName);
            for (String imp : imports) {
                if (imp.contains("com.tenx.transactionmonitoringadapter"))
                    graph.addLink(clasName, imp.substring(imp.lastIndexOf(".")+1));
            }
        }
        return graph.toString();
    }

    private String generateScriptForPackageDiagram(String fileFullPath){
        String res = "";

        // Prepare the reader
        JavaProjectBuilder builder = new JavaProjectBuilder();
        builder.addSourceTree(new File(fileFullPath));

        // create the object Graph
        PackageDiagramBuilderV1 graph = new PackageDiagramBuilderV1();

        // Iterate through the file (classes source code) and construct the output
        Iterator iter = builder.getSources().iterator();
        while (iter.hasNext()) {

            // get one source file xyz.java
            JavaSource src = (JavaSource) iter.next();

            List<String> imports = src.getImports();
            String pkgName = src.getClasses().stream().findFirst().orElse(null).getPackageName();

            for (String imp : imports) {
                if (imp.contains("com.tenx.fraudamlmanager")) {
                    String pkgRef = builder.getClassByName(imp).getPackageName();
                    if(pkgRef != "")
                        graph.addLink(pkgName, pkgRef);
                }
            }
        }

        return graph.toString();
    }

}
