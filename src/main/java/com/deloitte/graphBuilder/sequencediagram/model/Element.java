package com.deloitte.graphBuilder.sequencediagram.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.List;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "_classDescription",
    "_methodName",
    "_attributes",
    "_argNames",
    "_argTypes",
    "_returnType",
    "_hashCode"
})
@Data
public class Element {

    @JsonProperty("_classDescription")
    public ClassDescription classDescription;
    @JsonProperty("_methodName")
    public String methodName;
    @JsonProperty("_attributes")
    public List<String> attributes = null;
    @JsonProperty("_argNames")
    public List<String> argNames = null;
    @JsonProperty("_argTypes")
    public List<String> argTypes = null;
    @JsonProperty("_returnType")
    public String returnType;
    @JsonProperty("_hashCode")
    public Integer hashCode;

}