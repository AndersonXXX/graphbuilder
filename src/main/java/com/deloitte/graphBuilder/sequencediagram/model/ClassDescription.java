package com.deloitte.graphBuilder.sequencediagram.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.List;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "_className",
    "_attributes"
})
@Data
public class ClassDescription {

    @JsonProperty("_className")
    public String className;
    @JsonProperty("_attributes")
    public List<String> attributes;

}