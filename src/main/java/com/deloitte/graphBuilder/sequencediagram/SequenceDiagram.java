package com.deloitte.graphBuilder.sequencediagram;

import com.deloitte.graphBuilder.model.LinkShape;
import com.deloitte.graphBuilder.model.Shape;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SequenceDiagram {
    private List<Shape> shapes = new ArrayList<>();
    private List<LinkShape> links = new ArrayList<>();

    public void addShape(Shape aShape){
        if (shapes.stream().filter(s -> s.getName().equals(aShape.getName())).findFirst().equals(Optional.empty())) {
            aShape.setPosition(shapes.size()+1);
            shapes.add(aShape);
        }
    }

    public void addLink(LinkShape aLink){

    }
}
