package com.deloitte.graphBuilder.sequencediagram;

import com.deloitte.graphBuilder.model.LinkShape;
import com.deloitte.graphBuilder.model.Shape;
import com.deloitte.graphBuilder.packagediagram.InPayload;
import com.deloitte.graphBuilder.service.IDiagram;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("SequenceDiagramBuilder")
public class SequenceDiagramBuilder implements IDiagram {

    private SequenceDiagram diagram = new SequenceDiagram();

    public void init(InPayload params){
        diagram = new SequenceDiagram();
    }

    //************************************************************
    public void addShape(Shape aShape){
        diagram.addShape(aShape);
    }

    public void addLink(LinkShape aLink){

    }
    // ****************
    // *** IDiagram ***
    // ****************
    public String getDiagram(){
        StringBuilder builder = new StringBuilder();
        builder.append("sequenceDiagram").append(System.lineSeparator());

        for (Shape shape : diagram.getShapes()) {
            builder.append("participant ").append(shape.getPosition()).append(" as ").append(shape.getName())
                .append(System.lineSeparator());
        }

        for (LinkShape link : diagram.getLinks()) {

        }

        return builder.toString();
    }

}
