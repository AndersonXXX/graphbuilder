package com.deloitte.graphBuilder.sequencediagram;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sequence")
@RequiredArgsConstructor
@Api(tags = "Sequence")
@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
public class SequenceDiagramController {

    private final SequenceDiagramProcessor processor;

    @ApiOperation(value = "Endpoint to generate the sequence diagram of one method")
    @PostMapping("/getSequenceDiagram")
    public String getModuleDiagram(@RequestBody @Valid SequencePayload params){
        System.out.println("Call received - getSequenceDiagram");
        processor.process(params);
        return processor.getDiagram();
    }
}
