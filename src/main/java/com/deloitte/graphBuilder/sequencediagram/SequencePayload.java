package com.deloitte.graphBuilder.sequencediagram;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class SequencePayload {
    @NotNull
    @ApiModelProperty(example = "/Users/mdellarovere/Documents/Clients/10x/diagrams/SDExport/SD-graphBuilder3.txt")
    private String sourcePath;
}
