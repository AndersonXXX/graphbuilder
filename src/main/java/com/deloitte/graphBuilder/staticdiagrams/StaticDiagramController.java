package com.deloitte.graphBuilder.staticdiagrams;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/static")
@RequiredArgsConstructor
@Api(tags = "static Diagrams")
public class StaticDiagramController {

    private final StaticDiagramService staticDiagramService;


    @ApiOperation(value = "Endpoint to retrieve a static, previously generated diagram")
    @PostMapping("/getStaticDiagram")
    public String getStaticDiagram(@RequestBody @Valid String fileName){
        System.out.println("Call received - getStaticDiagram");

        return staticDiagramService.getDiagram(fileName);
    }

    @ApiOperation(value = "Endpoint to retrieve the list of all static diagrams generated previously")
    @PostMapping("/getStaticDiagramsList")
    public List<String> getStaticDiagramsList(){
        System.out.println("Call received - getStaticDiagramsList");

        return staticDiagramService.getDiagramList();
    }
}
