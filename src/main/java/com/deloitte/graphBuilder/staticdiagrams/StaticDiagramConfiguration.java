package com.deloitte.graphBuilder.staticdiagrams;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "static")
@Data
public class StaticDiagramConfiguration {
    private String folderPath;

}
