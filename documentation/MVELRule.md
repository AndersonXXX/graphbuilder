## Introduction
In this project MVEL is used to write the condition and the actions of the rule.  

Have a look to the [DocumentationPage](http://mvel.documentnode.com/)  

The **Condition** will be mainly executed over the **Source** putting some condition on Java class properties and methods.  
The **Actions** will be used to manipulate the **Diagram**  

In the Facts object are added a `diagram` and a `class` for every iteration.  
Where `diagram` is an instance of `DiagramV2` ([Here](../src/main/java/com/deloitte/graphBuilder/v2/diagrams/DiagramV2.java)) 
and `class` is an instance of `JavaClass` (Miss a valid Wrapper. It is pointing to the model object of Qdox).  

NOTE!!! the MVEL rule allow to call all the get method with a different syntax, more easy than Java.  
For example, a call like `someObj.getProperty().getSomeInnerProperty()` can be written this way `someObj.Property.SomeInnerProperty`.  

This means that looking at the class definition of `DiagramV2` ([Here](../src/main/java/com/deloitte/graphBuilder/v2/diagrams/DiagramV2.java))
it is possible to predict what functionality are available.  
The same is not valid for the class `JavaClass` defined in QDOX because at the moment it is injected a JavaSource, but wrapped as a `JavaClass`.
In the next version it will be fixed.  

## Usage
MVEL is a programming language, business oriented. It has the same basic abilities of any other programming language.  
For now we will document the use of declare variables, execute instruction, make choices with `if-then-else` and iterate through elements with `foreach`, `while` and `do-while` statements.  
Refer to the [DocumentationPage](http://mvel.documentnode.com/) to have more details regarding MVEL language.  

### if-then-else 
MVEL has capabilities to take decisions. the if-then-else statement has the same syntax of Java.  

Here an example:  
```
if (var > 0) {
   System.out.println("Greater than zero!");
}
else if (var == -1) { 
   System.out.println("Minus one!");
}
else { 
   System.out.println("Something else!");
}
```  

### foreach (loop)
In MVEL it is not needed to specify types.  
As you can see from the example below, the attribute `name` doesn't specify the type. This make rule writing more easy.  

Example:  
```
count = 0;
foreach (name : people) {
   count++;
   System.out.println("Person #" + count + ":" + name);
}
```  

### for loop

Example:  
```
for (int i =0; i < 100; i++) { 
   System.out.println(i);
}
```  

### do while

Example:  
```
do { 
   x = something();
} 
while (x != null);
```  

### while

Example:  
```
while (isTrue()) {
   doSomething();
}
```  

### Manipulate Code
`class.Name` return a `String` containing the name of the class.  
`class.Methods` return a `List<JavaMethod>` containing the methods of the class.  
`class.Imports` return a `List<String>` containing the imports of the class.  
`class.Constructors` return a `List<JavaConstructor>` containing the constructors of the class.  
`class.Annotations` return a `List<JavaAnnotation>` containing the Annotations of the class.  
`class.Fields` return a `List<JavaField>`  containing the fields of the class.  


### Manipulate Diagram
`diagram.addShape(int position, String id, String name)` add a box shape to the diagram model.  
`diagram.addLink(String name, String sourceNode, String destNode)` add a link to the diagram model.  
 
### Foreach
an example showing how to iterate through a list.  
`foreach (constructor : class.Constructors){ ... }`