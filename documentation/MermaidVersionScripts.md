## Mermaid diagrams

###### V1.0.2  
- Added DB  
- Added SubGraph  
- Changed package structure to have Adapters (to be completed)  

###### V1.0.1  
- Added git capability. Code is cloned if not present in workspace.  
- Added Prometheus and Grafana for Application Monitoring  
- Added MermaidHelper to format the Mermaid script (add tabulations to subgraph)

Image ![MermaidDiagram](images/MermaidDiagramV1.0.1.png)  

Script:
```
graph LR;
subgraph f71878ee-351f-4c04-8a25-f55cf136312b[com]
	com[com];
	subgraph e1deb088-a443-46de-8eff-c364d6b16a39[deloitte]
		com.deloitte[deloitte];
		subgraph a8ae8543-f9ee-4603-a1a2-8aaae8c40d3d[graphBuilder]
			com.deloitte.graphBuilder[graphBuilder];
			subgraph 2fb4d4d7-4413-450b-bb51-aa1ddfd17cd3[v2]
				com.deloitte.graphBuilder.v2[v2];
				subgraph 286ae6dd-9931-4f9c-a28b-d5062349d8e3[serialisers]
					com.deloitte.graphBuilder.v2.serialisers[serialisers];
				end
				subgraph eac11faf-87a8-4ee8-9948-0f40b62b4704[diagrams]
					com.deloitte.graphBuilder.v2.diagrams[diagrams];
				end
				subgraph 05f38d02-93ce-4616-8ca6-72a1e0072f31[utils]
					com.deloitte.graphBuilder.v2.utils[utils];
				end
				subgraph a63b1d34-b58c-4605-acee-bd2cb451f833[exceptions]
					com.deloitte.graphBuilder.v2.exceptions[exceptions];
				end
				subgraph 4d6e14d3-55f0-464d-8f5a-dc2947e8cd44[model]
					com.deloitte.graphBuilder.v2.model[model];
				end
				subgraph 15798ba3-83d2-4984-b95a-6788211af0b2[rules]
					com.deloitte.graphBuilder.v2.rules[rules];
				end
				subgraph df604453-cc91-4227-915b-54eeb28f72cb[readers]
					com.deloitte.graphBuilder.v2.readers[readers];
				end
				subgraph 98070e96-c138-4b0e-a76b-de5ef844129e[interfaces]
					com.deloitte.graphBuilder.v2.interfaces[interfaces];
				end
				subgraph 2e33766f-8a56-4662-83ad-017abac54ba7[mveldiagram]
					com.deloitte.graphBuilder.v2.mveldiagram[mveldiagram];
				end
			end
		end
	end
end
com.deloitte.graphBuilder.v2.mveldiagram-->com.deloitte.graphBuilder.v2.interfaces;
com.deloitte.graphBuilder.v2.readers-->com.deloitte.graphBuilder.v2.exceptions;
com.deloitte.graphBuilder.v2.serialisers-->com.deloitte.graphBuilder.v2.interfaces;
com.deloitte.graphBuilder.v2.readers-->com.deloitte.graphBuilder.v2.utils;
com.deloitte.graphBuilder.v2.mveldiagram-->com.deloitte.graphBuilder.v2.model;
com.deloitte.graphBuilder.v2.interfaces-->com.deloitte.graphBuilder.v2.diagrams;
com.deloitte.graphBuilder.v2.interfaces-->com.deloitte.graphBuilder.v2.mveldiagram;
com.deloitte.graphBuilder.v2.mveldiagram-->com.deloitte.graphBuilder.v2.rules;
com.deloitte.graphBuilder.v2.mveldiagram-->com.deloitte.graphBuilder.v2.readers;
com.deloitte.graphBuilder.v2-->com.deloitte.graphBuilder.v2.exceptions;
com.deloitte.graphBuilder.v2.mveldiagram-->com.deloitte.graphBuilder.v2.exceptions;
com.deloitte.graphBuilder.v2.mveldiagram-->com.deloitte.graphBuilder.v2.diagrams;
com.deloitte.graphBuilder.v2.rules-->com.deloitte.graphBuilder.v2.exceptions;
com.deloitte.graphBuilder.v2.rules-->com.deloitte.graphBuilder.v2.mveldiagram;
com.deloitte.graphBuilder.v2.serialisers-->com.deloitte.graphBuilder.v2.diagrams;
com.deloitte.graphBuilder.v2.readers-->com.deloitte.graphBuilder.v2;
```
###### V1.0.0  
MVEL rule with complex Condition and multiple action. Accept multiple Rules.  

Image ![MermaidDiagram](images/MermaidDiagramV1.0.0.png)   

Script:
```
graph LR;
subgraph e2169351-2bfb-48c4-a74e-09bb7a58ab41[com]
com[com];
subgraph 19d50e4f-8923-447a-bfa3-00c55d00be76[deloitte]
com.deloitte[deloitte];
subgraph 8933b95d-794b-4385-a4fe-4f87607d6e0e[graphBuilder]
com.deloitte.graphBuilder[graphBuilder];
subgraph a73390d8-7c03-4659-be17-20392c23d9fc[v2]
com.deloitte.graphBuilder.v2[v2];
subgraph 3995e98a-45b1-420d-821b-4da59042e72b[serialisers]
com.deloitte.graphBuilder.v2.serialisers[serialisers];
end
subgraph 137a3383-b816-42fe-b1db-e6e1d868ac64[diagrams]
com.deloitte.graphBuilder.v2.diagrams[diagrams];
end
subgraph d96573b4-eb9b-4693-bf67-9ff09d40f02b[exceptions]
com.deloitte.graphBuilder.v2.exceptions[exceptions];
end
subgraph 61a6941f-fe89-40e4-a2bc-c97b843e1872[rules]
com.deloitte.graphBuilder.v2.rules[rules];
end
subgraph c65f473e-32e1-4458-99f7-f0703cfb9c26[interfaces]
com.deloitte.graphBuilder.v2.interfaces[interfaces];
end
subgraph 5be8f0ea-3a55-4a03-b83e-46ffb635129b[mveldiagram]
com.deloitte.graphBuilder.v2.mveldiagram[mveldiagram];
end
end
end
end
end
com.deloitte.graphBuilder.v2.mveldiagram-->com.deloitte.graphBuilder.v2.interfaces;
com.deloitte.graphBuilder.v2.serialisers-->com.deloitte.graphBuilder.v2.interfaces;
com.deloitte.graphBuilder.v2.mveldiagram-->com.deloitte.graphBuilder.v2;
com.deloitte.graphBuilder.v2.mveldiagram-->com.deloitte.graphBuilder.v2.exceptions;
com.deloitte.graphBuilder.v2.interfaces-->com.deloitte.graphBuilder.v2.diagrams;
com.deloitte.graphBuilder.v2.mveldiagram-->com.deloitte.graphBuilder.v2.diagrams;
com.deloitte.graphBuilder.v2.rules-->com.deloitte.graphBuilder.v2.exceptions;
com.deloitte.graphBuilder.v2.rules-->com.deloitte.graphBuilder.v2.mveldiagram;
com.deloitte.graphBuilder.v2.interfaces-->com.deloitte.graphBuilder.v2.mveldiagram;
com.deloitte.graphBuilder.v2.serialisers-->com.deloitte.graphBuilder.v2.diagrams;
com.deloitte.graphBuilder.v2.mveldiagram-->com.deloitte.graphBuilder.v2.rules;
```

###### V0.0.1  
Image ![MermaidDiagram](images/MermaidDiagramV0.0.1.png)    

Script:
```
graph LR;
subgraph 04132973-c678-4074-83e1-8b4aba18c1d6[com]
 com[com];
 subgraph 7305d564-46e2-48e8-a2c6-d69e45d373b2[deloitte]
  com.deloitte[deloitte];
  subgraph fdde9dec-8b61-4b56-a71e-fd3e7904eca0[graphBuilder]
   com.deloitte.graphBuilder[graphBuilder];
   subgraph a192bce3-0607-41a3-8f49-a28a0a3cdccb[v2]
    com.deloitte.graphBuilder.v2[v2];
    subgraph bad96f27-86e5-497c-8af8-7b1cee9b3090[rules]
     com.deloitte.graphBuilder.v2.rules[rules];
    end
    subgraph 0001[diagrams]
     com.deloitte.graphBuilder.v2.diagrams[diagrams];
    end
    subgraph 0002[exceptions]
     com.deloitte.graphBuilder.v2.exceptions[exceptions];
    end
    subgraph 0003[interfaces]
     com.deloitte.graphBuilder.v2.interfaces[interfaces];
    end
    subgraph 0004[mveldiagram]
     com.deloitte.graphBuilder.v2.mveldiagram[mveldiagram];
    end
    subgraph 0005[serialisers]
     com.deloitte.graphBuilder.v2.serialisers[serialisers];
    end
   end
  end
 end
end
com.deloitte.graphBuilder.v2.mveldiagram-->com.deloitte.graphBuilder.v2.interfaces;
com.deloitte.graphBuilder.v2.serialisers-->com.deloitte.graphBuilder.v2.interfaces;
com.deloitte.graphBuilder.v2.mveldiagram-->com.deloitte.graphBuilder.v2;
com.deloitte.graphBuilder.v2.mveldiagram-->com.deloitte.graphBuilder.v2.exceptions;
com.deloitte.graphBuilder.v2.interfaces-->com.deloitte.graphBuilder.v2.diagrams;
com.deloitte.graphBuilder.v2.mveldiagram-->com.deloitte.graphBuilder.v2.diagrams;
com.deloitte.graphBuilder.v2.rules-->com.deloitte.graphBuilder.v2.exceptions;
com.deloitte.graphBuilder.v2.rules-->com.deloitte.graphBuilder.v2.mveldiagram;
com.deloitte.graphBuilder.v2.interfaces-->com.deloitte.graphBuilder.v2.mveldiagram;
com.deloitte.graphBuilder.v2.serialisers-->com.deloitte.graphBuilder.v2.diagrams;
com.deloitte.graphBuilder.v2.mveldiagram-->com.deloitte.graphBuilder.v2.rules;
```