# ChangeLog

#### V1.0.2
- Added DB capabilities to save MVEL rules on DB
- Added flag to save payload on DB  
- Added end point to retrieve a list of Payload previously saved on DB
- Added end point to retrieve a list of Branches on a remote repo  
- Added functionality to retrieve a list of Local branches (not mapped yet)  
 
#### V1.0.1  
- Added git capability. Code is cloned if not present in workspace.  
- Added Prometheus and Grafana for Application Monitoring  
- Added MermaidHelper to format the Mermaid script (add tabulations to subgraph)  

#### V1.0.0  
- MVEL rule with complex Condition and multiple action. Accept multiple Rules.  
