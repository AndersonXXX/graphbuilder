# Grafana
Grafana is used to monitor the application

## Metrics exposed
- `modulediagramv2.fetch.success` -> expose the number of successful calls to MVEL diagram endpoint
- `modulediagramv2.fetch.fail` -> expose the number of failed calls to MVEL diagram endpoint
