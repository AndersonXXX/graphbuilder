# PR Checklist
- change version in [build.gradle](../../build.gradle)  
- DON'T COMMIT application-dev.yml  
- Check metrics  
- Export diagram from correct branch!  
- Update documentation with new valid payload in [Model](Model.md)  
- Update [Changelog](../ChangeLog.md)  
- Update [MermaidVersionScripts](../MermaidVersionScripts.md)  
- Update [TODO](../TODO.md)  
