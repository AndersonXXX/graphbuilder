## Model Description

##### DiagramV2
has a list of shape and a list of lines.  

snippet:
```java
class DiagramV2 {
    List<ShapeV2> shapes;
    List<LinkShapeV2> links;
}
```

### Payloads
Here a list of valid payloads (both works but currently only JSON format is accepted)  

###### V1.0.3  
Added functionality to generate diagram of MD files. a shape for each file and an arrow for each referenced document.  


```json
{
  "mvelRuleList": [
{
      "actions": [
        "diagram.addNodeWithPath(file.RelativePath, \"/\")",
"foreach (link : file.LinksMD){ diagram.addLink(\"point to\", file.RelativePath, link.Folder + link.FileName) }"
      ],
      "condition": "true",
      "description": "This rule generate a diagram of linked MD file. Helps to track how the file are linked each other",
      "name": "Generate MD links diagram",
      "priority": 1
    }
  ],
  "pkgDefault": "com.deloitte.graphBuilder.v2",
  "projName": "GraphBuilder",
  "sourcePath": "/Users/mdellarovere/Projects/workspace/fraud-aml-manager/src/main/java/com/tenx/fraudamlmanager",
  "rootPath": "/src/main/java/com/deloitte/graphBuilder/v2",
  "repositoryURL": "https://AndersonXXX@bitbucket.org/AndersonXXX/graphbuilder.git",
  "branchName": "feature/diagram",
  "savePayload": "true"
}
```  

```json
{
  "mvelRuleList": [
{
      "actions": [
        "diagram.addShape(0, file.raw.Name, file.raw.Name)",
"foreach (link : file.LinksMD){ diagram.addLink(\"point to\", file.raw.Name, link) }"
      ],
      "condition": "true",
      "description": "This rule generate a diagram of linked MD file. Helps to track how the file are linked each other",
      "name": "Generate MD links diagram",
      "priority": 1
    }
  ],
  "pkgDefault": "com.deloitte.graphBuilder.v2",
  "projName": "GraphBuilder",
  "sourcePath": "/Users/mdellarovere/Projects/workspace/fraud-aml-manager/src/main/java/com/tenx/fraudamlmanager",
  "rootPath": "/src/main/java/com/deloitte/graphBuilder/v2",
  "repositoryURL": "https://AndersonXXX@bitbucket.org/AndersonXXX/graphbuilder.git",
  "branchName": "feature/diagram",
  "savePayload": "true"
}
```

###### V1.0.2  
Added flag to save on DB  

Generate SubGraphDiagram:  
```json
{
  "mvelRuleList": [
{
      "actions": [
        "subdiagram.addPackage(source.PackageName)", "subdiagram.addClass(source)"
      ],
      "condition": "source.PackageName.contains(\"com.deloitte.graphBuilder.v2\")",
      "description": "Creation of the Waterfall node (this action fire only one as it check a class name)",
      "name": "Waterfall node",
      "priority": 1
    }
  ],
  "pkgDefault": "com.deloitte.graphBuilder.v2",
  "projName": "GraphBuilder",
  "sourcePath": "/Users/mdellarovere/Projects/workspace/fraud-aml-manager/src/main/java/com/tenx/fraudamlmanager",
  "rootPath": "/src/main/java/com/deloitte/graphBuilder/v2",
  "repositoryURL": "https://AndersonXXX@bitbucket.org/AndersonXXX/graphbuilder.git",
  "branchName": "feature/diagram",
  "savePayload": "true"
}
```  

Generate simple diagram (module diagram):  
```json
{
  "mvelRuleList": [
    {
      "actions": [
        "diagram.addShape(0, class.Name, class.Name, \"#f9f\")",
"foreach (method : class.Methods){ diagram.addLink(method.getCallSignature(), class.Name, \"DiagramV2\") }"
      ],
      "condition": "source.Imports.contains(\"com.deloitte.graphBuilder.v2.diagrams.DiagramV2\")",
      "description": "Check after putting in Facts the class and the source",
      "name": "Check for Imports",
      "priority": 3
    },
    {
      "actions": [
        "diagram.addShape(0, class.Name.substring(0, class.Name.indexOf(\"Exception\")), class.Name.substring(0, class.Name.indexOf(\"Exception\")))",
"foreach (constructor : class.Constructors){ diagram.addLink(constructor.getCallSignature(), \"Waterfall\", class.Name.substring(0, class.Name.indexOf(\"Exception\"))); }"
      ],
      "condition": "class.Name.contains(\"Exception\")",
      "description": "Creation of a shape for each class that contains Reader in name",
      "name": "Reader Shapes creation",
      "priority": 2
    },
    {
      "actions": [
        "diagram.addShape(0, \"Waterfall\", \"Waterfall\")"
      ],
      "condition": "class.Name.equals(\"MVELParsingException\")",
      "description": "Creation of the Waterfall node (this action fire only one as it check a class name)",
      "name": "Waterfall node",
      "priority": 1
    }
  ],
  "pkgDefault": "com.deloitte.graphBuilder.v2",
  "projName": "GraphBuilder",
  "sourcePath": "/Users/mdellarovere/Projects/workspace/fraud-aml-manager/src/main/java/com/tenx/fraudamlmanager",
  "rootPath": "/src/main/java/com/deloitte/graphBuilder/v2",
  "repositoryURL": "https://AndersonXXX@bitbucket.org/AndersonXXX/graphbuilder.git",
  "branchName": "master",
  "savePayload": "true"
}
```
###### V1.0.1  
Added git capability. Code is cloned if not present in workspace.  

```Json
{
  "mvelRuleList": [
{
      "actions": [
        "diagram.addShape(0, class.Name, class.Name, \"#f9f\")",
"foreach (method : class.Methods){ diagram.addLink(method.getCallSignature(), class.Name, \"DiagramV2\") }"
      ],
      "condition": "source.Imports.contains(\"com.deloitte.graphBuilder.v2.diagrams.DiagramV2\")",
      "description": "Check after putting in Facts the class and the source",
      "name": "Check for Imports",
      "priority": 3
    },
    {
      "actions": [
        "diagram.addShape(0, class.Name.substring(0, class.Name.indexOf(\"Exception\")), class.Name.substring(0, class.Name.indexOf(\"Exception\")))",
"foreach (constructor : class.Constructors){ diagram.addLink(constructor.getCallSignature(), \"Waterfall\", class.Name.substring(0, class.Name.indexOf(\"Exception\"))); }"
      ],
      "condition": "class.Name.contains(\"Exception\")",
      "description": "Creation of a shape for each class that contains Reader in name",
      "name": "Reader Shapes creation",
      "priority": 2
    },
{
      "actions": [
        "diagram.addShape(0, \"Waterfall\", \"Waterfall\")"
      ],
      "condition": "class.Name.equals(\"MVELParsingException\")",
      "description": "Creation of the Waterfall node (this action fire only one as it check a class name)",
      "name": "Waterfall node",
      "priority": 1
    }
  ],
  "pkgDefault": "com.deloitte.graphBuilder.v2",
  "projName": "GraphBuilder",
  "sourcePath": "/Users/mdellarovere/Projects/workspace/fraud-aml-manager/src/main/java/com/tenx/fraudamlmanager",
  "rootPath": "/src/main/java/com/deloitte/graphBuilder/v2",
  "repositoryURL": "https://AndersonXXX@bitbucket.org/AndersonXXX/graphbuilder.git",
  "savePayload": "true"
}
```

###### V1.0.0  
MVEL rule with complex Condition and multiple action. Accept multiple Rules.  

```Json
{
  "mvelRuleList": [
    {
      "actions": [
        "diagram.addShape(0, class.getName().substring(0, class.getName().indexOf(\"Client\")), class.getName().substring(0, class.getName().indexOf(\"Client\")))",
"foreach (com.thoughtworks.qdox.model.JavaMethod method : class.getMethods()){ diagram.addLink(method.getCallSignature(), \"FraudAMLManager\", class.getName().substring(0,class.getName().indexOf(\"Client\"))); }"
      ],
      "condition": "class.getImports().contains(\"org.springframework.cloud.openfeign.FeignClient\")",
      "description": "Creation of a shape for each class that contains Feign Client import",
      "name": "Client Shapes creation",
      "priority": 2
    },
{
      "actions": [
        "diagram.addShape(0, \"FraudAMLManager\", \"FraudAMLManager\")"
      ],
      "condition": "class.getName().equals(\"FraudAmlManagerApplication\")",
      "description": "Creation of the FraudAMLManager node (this action fire only one as it check a class name)",
      "name": "FAM node",
      "priority": 1
    }
  ],
  "pkgDefault": "com.tenx.fraudamlmanager.infrastructure",
  "projName": "fraudamlmanager",
  "sourcePath": "/Users/mdellarovere/Projects/workspace/fraud-aml-manager/src/main/java/com/tenx/fraudamlmanager"
}
```

#### YAML payload
```json
{
  "mvelRuleList": [
    {
      "actions": [
        "diagram.getShapes().add(new com.deloitte.graphBuilder.v2.diagrams.ShapeV2(0, class.getName().substring(0, class.getName().indexOf(\\\"Client\\\"))))",
"foreach (com.thoughtworks.qdox.model.JavaMethod method : class.getMethods()) {diagram.getLinks().add(com.deloitte.graphBuilder.v2.diagrams.LinkShapeV2.builder().name(method.getCallSignature()).sourceNode(\\\"FraudAMLManager\\\").destNode(class.getName().substring(0, class.getName().indexOf(\\\"Client\\\"))).build());}"
      ],
      "condition": "class.getSource().getImports().contains(\\\"org.springframework.cloud.openfeign.FeignClient\\\")",
      "description": "first MVEL test",
      "name": "mvel rule",
      "priority": 1
    }
  ],
  "pkgDefault": "com.tenx.fraudamlmanager",
  "projName": "fraudamlmanager",
  "sourcePath": "/Users/mdellarovere/Projects/workspace/fraud-aml-manager/src/main/java/com/tenx/fraudamlmanager/infrastructure"
}
```