# Note

## Startup application
In order to use full functionalities it is needed to startup some service  

### Prometheus
#### Install
`docker pull prom/prometheus`  
#### Start
`docker run -d --name=prometheus -p 9090:9090 -v /Users/mdellarovere/Projects/workspace/graphBuilder/src/main/resources/prometheus.yml:/etc/prometheus/prometheus.yml prom/prometheus --config.file=/etc/prometheus/prometheus.yml`  
Verify Prometheus has mapped your app: `http://localhost:9090/targets`  
Check Prometheus graph: `http://localhost:9090/graph`  

Once it was ran, to restart use this command: `docker start /prometheus`  

### Grafana
#### Install
`brew install grafana`  

#### Start
`docker run -d --name=grafana -p 3000:3000 grafana/grafana`  
go to: `http://localhost:3000/` and update settings to point to your Prometheus instance. Then start shaping dashboards.  

Once it was ran, to restart use this command: `docker start /grafana`  


### Corckroach DB
#### Install
`brew install cockroachdb/tap/cockroach`  

#### Start
`cockroach start \
 --insecure \
 --store=node1 \
 --listen-addr=localhost:26257 \
 --http-addr=localhost:8089 \
 --join=localhost:26257,localhost:26258,localhost:26259 \
 --background`  
 
Initialise it:  
`cockroach init --insecure --host=localhost:26257`  

Terminate it:    
`cockroach quit --insecure --host=localhost:26257`

