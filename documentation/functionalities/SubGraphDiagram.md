# SubGraphDiagram
This section shows how to generate a SubGraph Diagram.  

Here it is a payload to generate a Package dependency diagram.  


```json
{
  "mvelRuleList": [
{
      "actions": [
        "subdiagram.addPackage(source.PackageName)", "subdiagram.addClass(source)"
      ],
      "condition": "source.PackageName.contains(\"com.deloitte.graphBuilder.v2\")",
      "description": "Creation of the Waterfall node (this action fire only one as it check a class name)",
      "name": "Waterfall node",
      "priority": 1
    }
  ],
  "pkgDefault": "com.deloitte.graphBuilder.v2",
  "projName": "GraphBuilder",
  "sourcePath": "/Users/mdellarovere/Projects/workspace/fraud-aml-manager/src/main/java/com/tenx/fraudamlmanager",
  "rootPath": "/src/main/java/com/deloitte/graphBuilder/v2",
  "repositoryURL": "https://AndersonXXX@bitbucket.org/AndersonXXX/graphbuilder.git",
  "branchName": "feature/diagram",
  "savePayload": "true"
}
```