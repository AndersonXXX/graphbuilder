#Module diagram
This section introduce you to the generation of a simple diagram.
It contains only nodes and links. Nodes cannot contain other nodes. To do this it is needed a Sub Graph diagram.

Here an example of how to generate module dependency diagram.  

Generate simple diagram (module diagram):  
```json
{
  "mvelRuleList": [
    {
      "actions": [
        "diagram.addShape(0, class.Name, class.Name, \"#f9f\")",
"foreach (method : class.Methods){ diagram.addLink(method.getCallSignature(), class.Name, \"DiagramV2\") }"
      ],
      "condition": "source.Imports.contains(\"com.deloitte.graphBuilder.v2.diagrams.DiagramV2\")",
      "description": "Check after putting in Facts the class and the source",
      "name": "Check for Imports",
      "priority": 3
    },
    {
      "actions": [
        "diagram.addShape(0, class.Name.substring(0, class.Name.indexOf(\"Exception\")), class.Name.substring(0, class.Name.indexOf(\"Exception\")))",
"foreach (constructor : class.Constructors){ diagram.addLink(constructor.getCallSignature(), \"Waterfall\", class.Name.substring(0, class.Name.indexOf(\"Exception\"))); }"
      ],
      "condition": "class.Name.contains(\"Exception\")",
      "description": "Creation of a shape for each class that contains Reader in name",
      "name": "Reader Shapes creation",
      "priority": 2
    },
    {
      "actions": [
        "diagram.addShape(0, \"Waterfall\", \"Waterfall\")"
      ],
      "condition": "class.Name.equals(\"MVELParsingException\")",
      "description": "Creation of the Waterfall node (this action fire only one as it check a class name)",
      "name": "Waterfall node",
      "priority": 1
    }
  ],
  "pkgDefault": "com.deloitte.graphBuilder.v2",
  "projName": "GraphBuilder",
  "sourcePath": "/Users/mdellarovere/Projects/workspace/fraud-aml-manager/src/main/java/com/tenx/fraudamlmanager",
  "rootPath": "/src/main/java/com/deloitte/graphBuilder/v2",
  "repositoryURL": "https://AndersonXXX@bitbucket.org/AndersonXXX/graphbuilder.git",
  "branchName": "master",
  "savePayload": "true"
}
```