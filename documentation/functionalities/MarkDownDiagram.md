# Mark Down diagram
This section shows you how to generate a diagram of MD files in a folder.  


This diagram shows the links between MD files. Easy to find not linked files.

#### Simple Diagram
This diagram shows also the folders where the files are  

```json
{
  "mvelRuleList": [
{
      "actions": [
        "diagram.addShape(0, file.raw.Name, file.raw.Name)",
"foreach (link : file.LinksMD){ diagram.addLink(\"point to\", file.raw.Name, link.FileName) }"
      ],
      "condition": "true",
      "description": "This rule generate a diagram of linked MD file. Helps to track how the file are linked each other",
      "name": "Generate MD links diagram",
      "priority": 1
    }
  ],
  "pkgDefault": "com.deloitte.graphBuilder.v2",
  "projName": "GraphBuilder",
  "sourcePath": "/Users/mdellarovere/Projects/workspace/fraud-aml-manager/src/main/java/com/tenx/fraudamlmanager",
  "rootPath": "/src/main/java/com/deloitte/graphBuilder/v2",
  "repositoryURL": "https://AndersonXXX@bitbucket.org/AndersonXXX/graphbuilder.git",
  "branchName": "master",
  "savePayload": "true"
}
```

#### SubGraph Diagram (NOT READY)
Flat structure of MD files with link to referenced files. Simple, no folders  

```json
{
  "mvelRuleList": [
{
      "actions": [
        "diagram.addNodeWithPath(file.RelativePath, \"/\")",
"foreach (link : file.LinksMD){ diagram.addLink(\"point to\", file.RelativePath, file.getRealPath(link)) }"
      ],
      "condition": "true",
      "description": "This rule generate a diagram of linked MD file. Helps to track how the file are linked each other",
      "name": "Generate MD links diagram",
      "priority": 1
    }
  ],
  "pkgDefault": "com.deloitte.graphBuilder.v2",
  "projName": "GraphBuilder",
  "sourcePath": "/Users/mdellarovere/Projects/workspace/fraud-aml-manager/src/main/java/com/tenx/fraudamlmanager",
  "rootPath": "/src/main/java/com/deloitte/graphBuilder/v2",
  "repositoryURL": "https://AndersonXXX@bitbucket.org/AndersonXXX/graphbuilder.git",
  "branchName": "master",
  "savePayload": "true"
}
```





SIMPLE

