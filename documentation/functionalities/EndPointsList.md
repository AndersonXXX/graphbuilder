# End points Documentation
Here is present the payload to generate a list of endpoints in the form:

TYPE - LocalPath

POST - /finChrimeCheck
GET - /Health

#### FAM endpoints
```json
{
  "mvelRuleList": [
    {
      "actions": [
        "foreach (method : class.getMethods()){ foreach (ann : method.getAnnotations() ) { if(ann.getType().getName().equals(\"PostMapping\")) { System.out.println(\"POST \" + ann.getProperty(\"value\")) } } }"
      ],
      "condition": "for (ann : class.getAnnotations()) { if (ann.getType().getName().equals(\"RestController\")) { return true } } return false",
      "description": "Creation of the Waterfall node (this action fire only one as it check a class name)",
      "name": "POST methods",
      "priority": 1
    },
{
      "actions": [
        "foreach (method : class.getMethods()){ foreach (ann : method.getAnnotations() ) { if(ann.getType().getName().equals(\"GetMapping\")) { System.out.println(\"GET \" + ann.getProperty(\"value\")) } } }"
      ],
      "condition": "for (ann : class.getAnnotations()) { if (ann.getType().getName().equals(\"RestController\")) { return true } } return false",
      "description": "Creation of the Waterfall node (this action fire only one as it check a class name)",
      "name": "GET methods",
      "priority": 2
    }
  ],
  "pkgDefault": "com.tenx.fraudamlmanager",
  "projName": "fraudamlmanager",
  "sourcePath": "/Users/mdellarovere/Projects/workspace/fraud-aml-manager/src/main/java/com/tenx/fraudamlmanager",
  "rootPath": "/src/main/java/com/tenx/fraudamlmanager",
  "repositoryURL": "https://bitbucket.10x.mylti3gh7p4x.net/scm/ft24/fraud-aml-manager.git",
  "branchName": "master",
  "savePayload": "true"
}
```

```json
{
  "mvelRuleList": [
    {
      "actions": [
        "foreach (method : class.getMethods()){ foreach (ann : method.getAnnotations() ) { if(ann.getType().getName().equals(\"PostMapping\")) { System.out.println(\"POST \" + ann.getProperty(\"value\")) } } }"
      ],
      "condition": "for (ann : class.getAnnotations()) { if (ann.getType().getName().equals(\"RestController\")) { return true } } return false",
      "description": "Creation of the Waterfall node (this action fire only one as it check a class name)",
      "name": "POST methods",
      "priority": 1
    },
{
      "actions": [
        "foreach (method : class.getMethods()){ foreach (ann : method.getAnnotations() ) { if(ann.getType().getName().equals(\"GetMapping\")) { System.out.println(\"GET \" + ann.getProperty(\"value\")) } } }"
      ],
      "condition": "for (ann : class.getAnnotations()) { if (ann.getType().getName().equals(\"RestController\")) { return true } } return false",
      "description": "Creation of the Waterfall node (this action fire only one as it check a class name)",
      "name": "GET methods",
      "priority": 2
    }
  ],
  "pkgDefault": "com.deloitte.graphBuilder.v2",
  "projName": "GraphBuilder",
  "sourcePath": "/Users/mdellarovere/Projects/workspace/fraud-aml-manager/src/main/java/com/tenx/fraudamlmanager",
  "rootPath": "/src/main/java/com/deloitte/graphBuilder/v2",
  "repositoryURL": "https://AndersonXXX@bitbucket.org/AndersonXXX/graphbuilder.git",
  "branchName": "master",
  "savePayload": "true"
}
```
#### GraphBuilder endpoints

```json
{
  "mvelRuleList": [
    {
      "actions": [
        "foreach (method : class.getMethods()){ foreach (ann : method.getAnnotations() ) { if(ann.getType().getName().equals(\"PostMapping\")) { System.out.println(\"POST \" + ann.getProperty(\"value\")) } } }"
      ],
      "condition": "for (ann : class.getAnnotations()) { if (ann.getType().getName().equals(\"RestController\")) { return true } } return false",
      "description": "Creation of the Waterfall node (this action fire only one as it check a class name)",
      "name": "POST methods",
      "priority": 1
    },
{
      "actions": [
        "foreach (method : class.getMethods()){ foreach (ann : method.getAnnotations() ) { if(ann.getType().getName().equals(\"GetMapping\")) { System.out.println(\"GET \" + ann.getProperty(\"value\")) } } }"
      ],
      "condition": "for (ann : class.getAnnotations()) { if (ann.getType().getName().equals(\"RestController\")) { return true } } return false",
      "description": "Creation of the Waterfall node (this action fire only one as it check a class name)",
      "name": "GET methods",
      "priority": 2
    }
  ],
  "pkgDefault": "com.deloitte.graphBuilder.v2",
  "projName": "GraphBuilder",
  "sourcePath": "/Users/mdellarovere/Projects/workspace/fraud-aml-manager/src/main/java/com/tenx/fraudamlmanager",
  "rootPath": "/src/main/java/com/deloitte/graphBuilder/v2",
  "repositoryURL": "https://AndersonXXX@bitbucket.org/AndersonXXX/graphbuilder.git",
  "branchName": "master",
  "savePayload": "true"
}
```