# ToDo  
- Create a ProcessReport to save with operation executed and data used (need a Rule Listener implementation to know what happens with Rules)  
- Integration for .md file library  
    - generation of diagram for .md structure  
- Java Parser: https://www.beyondjava.net/a-simple-class-parser  

- Integration of a Database (done)  
- PRIORITY: Update the SourceClass in order to map better the use of JavaClass and JavaSource. (Done. Removed class and exposed JavaSource)  
- PRIORITY: Add subgraph, colors and line type to model (Done)   